#ifndef _DATEHEURE_HPP_
#define _DATEHEURE_HPP_

#include <stdio.h>
#include <string>
using namespace std;

class heure_t {
public:
  signed char d,h;
  short m;
  void okise() { 
    while(m>=60) {
      h++;
      m-=60;
      if(h==24) {h=0;d++;}
    }
    while(m<0) {
      h--;
      m+=60;
      if(h==-1) {h=23;d--;}
    }
  }
  heure_t(int hh=0,int mm=0,int dd=0):d(dd),h(hh),m(mm) {okise();}
  operator string() const {
    char b[10];
    snprintf(b,10,"%2dh%02d",h,m);
    return string(b);
  }

  bool operator>(const heure_t &o) const {
    return d>o.d || (d==o.d && h>o.h)|| (d==o.d && h==o.h && m>o.m);
  }
  bool operator>=(const heure_t &o) const {
    return d>o.d || (d==o.d && h>o.h)|| (d==o.d && h==o.h && m>=o.m);
  }
  bool operator==(const heure_t &o) const {
    return (d==o.d && h==o.h && m==o.m);
  }
  bool operator!=(const heure_t &o) const {
    return !(o==*this);
  }
  bool operator<=(const heure_t &o) const {
    return o>=*this;
  }
  bool operator<(const heure_t &o) const {
    return o>*this;
  }
  
  int operator-(const heure_t &o) const {
    if(o>*this) return -(o-*this);
    return (m-o.m)+60*(h-o.h)+1440*(d-o.d);
  }

  heure_t operator+=(int i) {
    m+=i;
    okise();
    return *this;
  }
  heure_t operator+(int i) const {
    heure_t r=*this;
    r+=i;
    return r;
  }
  
  //bool operator>= ...

  string str() const {
    char b[20];
    snprintf(b,20,"%2dh%02d",h+24*d,m);
    return b;
  }
};


class date_t {
public:
  static bool bisext(int y) { return (y%4==0) && ((y%100)!=0 || (y%400)==0); }
  static int nbrjours(int y,int m) { 
    if(m==2) return bisext(y)?29:28;
    if(((m%2)==1 && m<=7) || ((m%2)==0 &&  m>=8)) return 31;
    return 30;
  }
  static int nbrjours(int y) { 
    return 365+(bisext(y)?1:0);
  }
  
  short y;
  signed char m,d;
  
public:
  date_t(int yy=0,int mm=0,int dd=0):y(yy),m(mm),d(dd) {}
  date_t(const date_t &o):y(o.y),m(o.m),d(o.d) {}
  const date_t &operator=(const date_t &o) {
    if(this!=&o) {
      y=o.y;m=o.m;d=o.d;
    }
    return *this;
  }

  int joursemaine() const {
    int d=(5+(*this-date_t(2000,1,1)))%7;
    if(d<0) d=(d+7)%7;
    return d;
  }
  
  string str() const {
    static const char *nomjours[7]={"Lun","Mar","Mer","Jeu","Ven","Sam","Dim"};
    char b[20];
    snprintf(b,20,"%s %d/%d/%d",nomjours[joursemaine()],d,m,y);
    return b;
  }

  string str2() const {
    static const char *nomjours[7]={"Lun","Mar","Mer","Jeu","Ven","Sam","Dim"};
    char b[20];
    snprintf(b,20,"%d/%d"/*,nomjours[joursemaine()]*/,d,m);
    return b;
  }
  
  date_t operator+=(int i) {
    if(i<0) return *this-=(-i);
    int dd=d;
    dd+=i;
    while(dd>nbrjours(y,m)) {
      dd-=nbrjours(y,m);
      m++; 
      if(m==13) { y++; m=1;}
    }
    d=dd;
    return *this;
  }

  date_t operator-=(int i) {
    if(i<0) return *this+=(-i);
    int dd=d;
    dd-=i;
    while(dd<=0) {
      m--;
      if(m==0) {y--;m=12;}
      dd+=nbrjours(y,m);
    }
    d=dd;
    return *this;
  }

  date_t operator+(int i) const {
    date_t r=*this;
    r+=i;
    return r;
  }

  date_t operator-(int i) const {
    date_t r=*this;
    r-=i;
    return r;
  }

  date_t operator++() {return (*this)+=1;}
  bool operator==(const date_t &o) const {
    return y==o.y && m==o.m && d==o.d;
  }
  bool operator!=(const date_t &o) const { return !(*this==o);}
  bool operator>(const date_t &o) const {
    return y>o.y  || (y==o.y && m>o.m) || (y==o.y && m==o.m && d>o.d);
  }
  bool operator>=(const date_t &o) const {
    return y>o.y  || (y==o.y && m>o.m) || (y==o.y && m==o.m && d>=o.d);
  }
  bool operator<=(const date_t &o) const {
    return o>=*this;
  }
  bool operator<(const date_t &o) const {
    return o>*this;
  }
  int operator-(const date_t &o) const {
    if(o<(*this)) return -(o-*this);
    int y_=y;
    int m_=m;
    //int d_=0;
    int r=-d;
    while(y_<o.y || (y_==o.y && m_<o.m)) {
      if(y_<o.y && m_==1) { // peut etre optimisé...
	r+=nbrjours(y_);
	y_++;
      } else {
	r+=nbrjours(y_,m_);
	m_++;if(m_==13){m_=1;y_++;}
      }
    }
    r+=o.d;
    return -r;
  }
  
};


class dateheure_t {
  date_t d;
  heure_t h;
  void okise() {
    d+=h.d;
    h.d=0;
  }

public:
  dateheure_t() {}
  dateheure_t(const date_t &dd):d(dd),h() {okise();}
  dateheure_t(const heure_t&hh):d(),h(hh) {okise();}
  dateheure_t(const date_t &dd,const heure_t&hh):d(dd),h(hh) {okise();}
  dateheure_t(const dateheure_t &o):d(o.d),h(o.h) {}

  
  const dateheure_t &operator=(const dateheure_t &o) {
    if(this!=&o) {
      d=o.d;h=o.h;
    }
    return *this;
  }

  dateheure_t operator+=(int i) {
    h+=i;
    okise();
    return *this;
  }

  heure_t &heure() { return h;}
  date_t &date() { return d;}
  const heure_t &heure() const { return h;}
  const date_t &date() const { return d;}

  //FIX!!!! heures d'ete/hiver!!!
  int operator-(const dateheure_t &o) const {
    return 1440*(d-o.d)+(h-o.h);
  }

  bool operator<(const dateheure_t &o) const {
    return o>*this;
  }
  bool operator>(const dateheure_t &o) const {
    return d>o.d  || (d==o.d && h>o.h);
  }
  bool operator>=(const dateheure_t &o) const {
    return d>o.d  || (d==o.d && h>=o.h);
  }
  bool operator==(const dateheure_t &o) const {
    return (d==o.d && h==o.h);
  }

  dateheure_t operator+(int i) const {
    dateheure_t u=*this;
    u+=i;
    return u;
  }

  void addheure(const heure_t &hh) {
    h=hh;
    okise();
  }

  string str() const {
    string r=d.str() + " " +h.str();
    return r;
  }

  string str2() const {
    string r=d.str2() + " " +h.str();
    return r;
  }
};
  
#endif
