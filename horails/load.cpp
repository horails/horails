#include "load.hpp"

bool getper(const string &s, date_t &d, int &nbr)
{
  _debug("-- periode BD --\n");
  {
    int in=open("FIC1",O_RDONLY);

    unsigned int i,j;
    
    unsigned int na=readint(in,4); // nbr de dessertes
    unsigned int nb=readint(in,4); // nbr de dessertes + trains
    unsigned int nc=readint(in,4); //9999999
    unsigned int nd=readint(in,2); //132 : nbr de gares avec passage
    skip(in,4);  // 1, 34
    unsigned int ne=readint(in,2); //5701 : nbr periodes
    unsigned int nf=readint(in,2); //3521 : nbr gares dans FIC1
    unsigned int nj=readint(in,2); // 155 : nombre de jours-1 dans la BD
    skip(in,6);  // 5, 0, 76 

    skip(in,4*na);
    
    skip(in,4*1440);
    skip(in,4);
    
    skip(in,2*nb);
    skip(in,2);
    
    skip(in,nb);
    skip(in,1);

    skip(in,4*nb);
    skip(in,4);
    
    {
      skip(in,2*(nf+1));
      skip(in,4);
      
      for(i=0;i<nd;i++) {
	skip(in,2+2*nd);
      }
      
    }
    
    {
      skip(in,2);
      
      int td[nj],ta[12];
      date_t tD[nj];
      
      for(i=0;i<nj-1;i++) {
	td[i]=readint(in,2);
	//printf("%d: %d\n",i,td[i]);
      }
      
      for(i=0;i<12;i++) {
	ta[i]=readint(in,2);
	//printf("%d: %d\n",i,td[i]);
      }

      for(i=0;i<nj-1;i++) {
	int jo=td[i]/100;
	int mo=td[i]%100;
	int an=ta[mo-1];
	tD[i]=date_t(an,mo,jo);
	if(i && tD[i]!=tD[i-1]+1) {
	  _warn("dates pas consécutives: %s %s\n",tD[i-1].str().c_str(),tD[i].str().c_str()); 
	}
      }
      
      d=tD[0];
      nbr=nj-1;
      
#if 0
      // le reste sert à quoi ??
      int p=lseek(in,0,SEEK_CUR);
      int f=lseek(in,0,SEEK_END);
      _debug("Le reste de FIC1 commence a %d et fait %d\n",p,f-p);
      lseek(in,p,SEEK_SET);

      for(i=0;i<f-p;i++) {
	j=readint(in,1);
	printf("%d: %d '%c'\n",i,j,j);
      }

#endif
   
    }
      
    close(in);
  }


}

bool load(dbs_t &dbs,db_t &db,const string &p) 
{
  date_t dbstart;
  int dbnbrjours;

  getper(p,dbstart,dbnbrjours);

  db.setper(dbstart,dbnbrjours);

  _log("trains du %s au %s\n",db.dbstart.str().c_str(),(db.dbstart+db.dbnbrjours-1).str().c_str());

  _debug("--gares--\n");
  {
    int fdgare=open("TRICODE",O_RDONLY);
    
    while(1) {
      try {
	string si=readstring(fdgare,7);
	int gcode=toint(si);
	string s=stripspaces(readstring(fdgare,23));
	//_debug("add gare %d '%s'\n",gid,s.c_str());
	skip(fdgare,2);
	db.gares->add(gcode,s);
      } catch (...) {
	break;
      }
    }
    
    close(fdgare);
  }   
  
  _debug("--gares 2--\n");
  {
    int fdgare=open("FICUIC",O_RDONLY);
    
    int ig=0;
    while(1) {
      try {
	string s=readstring(fdgare,7);
	skip(fdgare,2);
	gare_t *g_= db.gares->_gare[db.gares->_idgare[toint(s)]];
	if(g_) {
	  //gare_[ig]=g_;
	  db.gareid_[ig]=g_->id;
	}
	else 
	  _warn("erreur: gare '%s' inconnue\n",s.c_str());
	ig++;
      } catch (...) {
	break;
      }
    }
    
    close(fdgare);      
  }
  
  _debug("--pers--\n");
  {
    unsigned char bu[32];
    int fd=open("FICPER",O_RDONLY);
    
    int ig=0;
    while(1) {
      try {
	for(int m=0;m<32;m++) {
	  readchars(fd,1,bu+2*(m/2)+(1-(m%2)));
	}
	per_t *p=new per_t(db.dbstart,db.dbnbrjours);
	p->setper(bu);
	//_debug("%d:",ig);p->print();
	db.per_[ig]=p;
	db.per.insert(p);
	ig++;
      } catch (...) {
	break;
      }
    }
    
    close(fd);
  }
  
  
  _debug("--trains--\n");
  {
    
    int in=open("FIC1",O_RDONLY);
    
    unsigned int na=readint(in,4); // nbr de dessertes
    unsigned int nb=readint(in,4); // nbr de dessertes + trains
    unsigned int nc=readint(in,4); //9999999
    unsigned int nd=readint(in,2); //132 : nbr de gares avec passage
    skip(in,4);  // 1, 34
    unsigned int ne=readint(in,2); //5701 : nbr periodes
    unsigned int nf=readint(in,2); //3521 : nbr gares dans FIC1
    unsigned int nj=readint(in,2); // 155 : nombre de jours-1 dans la BD
    skip(in,6);  // 5, 0, 76 

    int taba[na];
    int tabmin[1440];
    int tabb[nb];
    heure_t tabbh[nb];
    unsigned char tabB[nb];
    
    _debug("-- indices heures --\n");
    
    int i,j;
    for(i=0;i<na;i++) {
      taba[i]=readint(in,4);
    }
    //_debug("%d %d %d ... %d %d %d\n",taba[0],taba[1],taba[2],taba[na-3],taba[na-2],taba[na-1]);
    
    _debug("-- heures --\n");
    
    for(i=0;i<1440;i++){
      tabmin[i]=readint(in,4);
    }

    for(i=0;i<1440;i++) {
      heure_t h(0,i);
      for(j=tabmin[i];j<((i+1==1440)?na:tabmin[i+1]);j++)
	tabbh[taba[j]]=h;
    }

    skip(in,4);
    
    _debug("-- id gares/trains --\n");
    
    for(i=0;i<nb;i++) {
      tabb[i]=readint(in,2);
    }
    
    skip(in,2);
    
    _debug("-- marqueurs trains --\n");
    
    for(i=0;i<nb;i++) {
      tabB[i]=readint(in,1);
    }
    
    skip(in,1);
    
    _debug("-- services/temps dessertes --\n");
    
    i=0;
    int n=0;
    while(i<nb) {
      int ii=i;
      
      train_t *t=new train_t();
      
      //_debug("heures 0: %s\n",tabbh[i].str().c_str());
      //_debug("heures 1: %s\n",tabbh[i+1].str().c_str());
      
      heure_t he=tabbh[i+1];
     
      per_t *p=db.per_[tabb[i]];
      if(p==NULL) _warn("periode %d non trouvée\n",tabb[i]);
      t->setper(p);
      //t->setpid(tabb[i]);
      
      try {
	
	int tid_=readint(in,4);
	i++;
	db.train_[tid_]=t;
	
	gare_t *b=db.gares->_gare[db.gareid_[tabb[i]]];
	if(b==NULL) _warn("gare %d non trouvée\n",tabb[i]);
	t->adddess(dess_t(he,0,b->getid()));
	
	int a21=readint(in,2); //services dans le train...
	int a22=readint(in,2);
	i++;
	
	int a3,a4;
	do {
	  a3=readint(in,2);
	  a4=readint(in,2);
	  
	  gare_t *b=db.gares->_gare[db.gareid_[tabb[i]]];
	  if(b==NULL) _warn("gare %d non trouvée\n",tabb[i]);
	  
	  //_debug("heures i: %s\n",tabbh[i].str().c_str());
	  he+=a4;
	  t->adddess(dess_t(he,a3,b->getid()));
	  he+=a3;
	  //if(i+1< nb && tabB[i+1]==0 && tabbh[i]!=he) warn ("heures different %s %s (d=%d %d)\n",he.str().c_str(),tabbh[i].str().c_str(),a4,a3);
	  i++;
	} while(i< nb && tabB[i]==0);
	
      } catch (...) {
	break;
      }
      
      n++;
    }

    skip(in,4);
    
    {
      _debug("-- connections --\n");

      /*
	per_t *per=new per_t(db.dbstart,db.dbnbrjours);
	per->setall();
	db.per.insert(per);
      */

      int ppg[nd];
      int pp[nd*nd];
	
      for(i=0;i<=nf;i++) {
	int b=readint(in,2);
	//if(b) printf("%d\n",i);
      }
      
      skip(in,4);
      
      for(i=0;i<nd;i++) {
	int id=readint(in,2);
	ppg[i]=id;
	for(j=0;j<nd;j++)
	  pp[i*nd+j]=readint(in,2);
      }
      
      for(i=0;i<nd;i++) {
	for(j=0;j<nd;j++) {
	  if(j!=i && pp[i*nd+j]<99) {
	    /*
	      string n=db.gares->garestring(db.gareid_[ppg[i]]);
	      string n2=db.gares->garestring(db.gareid_[ppg[j]]);
	      printf("%s <-> %s : %d\n",n.c_str(),n2.c_str(),pp[i*nd+j]);
	    */
	    if(pp[i*nd+j]>=1)
	      dbs.pass[db.gareid_[ppg[i]]][db.gareid_[ppg[j]]]=pp[i*nd+j];
	    else 
	      dbs.addsuper(db.gareid_[ppg[i]],db.gareid_[ppg[j]]);
	    //printf("%c",pp[i*nd+j]==99?' ':'A'+pp[i*nd+j]*25/99);
	  }
	}
	//printf("\n");
      }
    }

    skip(in,2*nj); //dates
    skip(in,24);  //années

    {
      
#if 1
      // le reste sert à quoi ??
      int p=lseek(in,0,SEEK_CUR);
      int f=lseek(in,0,SEEK_END);
      _debug("Le reste de FIC1 commence a %d et fait %d\n",p,f-p);
#endif

      // 5 chaines de caracteres terminées par 0
      // 76 blocs de 6 octets ???? 
   
    }
      
    close(in);
  }

  
  map<int,train_t*>::const_iterator itt;
  for(itt=db.train_.begin();itt!=db.train_.end();++itt) {
    for(int i=0;i<itt->second->size();i++) {
      //printf("%d: %d\n",i,(*(itt->second))[i].gare);
      dbs.gares._gare[(*(itt->second))[i].gare]->nbrtrains++;
    }
  }
  //dbs.inclevelgares(db);
  dbs.gares.inclevelgares();
  
#if 0

  // fixer l'insert avant de lire ca... (sinon retour dans le temps)
  
  _debug("--trains 2--\n");
  
  // FIC2 && FICGROUP
  {
    unsigned char bu[32];
    int fd=open("FIC2",O_RDONLY);
    int i,j;
    
    int maxx=readint(fd,2);
    int maxy=readint(fd,2);
    int maxz=readint(fd,2);
    int size=readint(fd,2);
    int ha=readint(fd,2);
    int hb=readint(fd,2);
    int hc=readint(fd,2);
    
    _debug2("*** %d %d %d %d %d %d %d ***\n",maxx,maxy,maxz,size,ha,hb,hc);
    
    vector<sfic2_t> vfc2;
    vfc2.resize(size);
    
    for(i=0;i<size;i++) {
      vfc2[i].a=readint(fd,2);
      vfc2[i].b=readint(fd,2);
      vfc2[i].c=readint(fd,2);
      vfc2[i].x=readint(fd,2);
      vfc2[i].y=readint(fd,2);
      vfc2[i].z=readint(fd,2);
    }
    
    close(fd);
    
    fd=open("FICGROUP",O_RDONLY);
    
    for(i=0;i<size;i++) {
      _debug2("== %d %d %d %d %d %d ==\n",vfc2[i].a,vfc2[i].b,vfc2[i].c,vfc2[i].x,vfc2[i].y,vfc2[i].z);
      _debug2(" gare %d : %s\n", vfc2[i].a, db.gares->garestring(db.gareid_[vfc2[i].a]).c_str());
      _debug2(" gare %d : %s\n", vfc2[i].b, db.gares->garestring(db.gareid_[vfc2[i].b]).c_str());
      _debug2(" gare %d : %s\n", vfc2[i].c, db.gares->garestring(db.gareid_[vfc2[i].c]).c_str());
      
      short tabid[vfc2[i].y];
      short tabmark[vfc2[i].y];
      //short tabyc[2*vfc2[i].y];
      int tabza[vfc2[i].z][2];
      
      
      heure_t tabheure[vfc2[i].y];
      {
	short tabmin[vfc2[i].x];
	int tabrefmin[vfc2[i].x];
	
	for(j=0;j<vfc2[i].x;j++) {
	  tabmin[j]=readint(fd,2);
	}
	//if(vfc2[i].x) _debug2("XA: %d -> %d\n",tabmin[0], tabmin[vfc2[i].x-1]);
	
	int m=0,M=0;
	for(j=0;j<vfc2[i].x;j++) {
	  tabrefmin[j]=readint(fd,4);
	  if(j==0) m=M=tabid[j];
	  if(m>tabrefmin[j]) m=tabrefmin[j];
	  if(M<tabrefmin[j]) M=tabrefmin[j];
	}
	//if(vfc2[i].x) _debug2("XB: %d ... %d\n",m,M);
	//if(vfc2[i].x) _debug2("XB: %d -> %d\n",tabrefmin[0], tabrefmin[vfc2[i].x-1]);
	
	for(j=0;j<vfc2[i].x;j++) {
	  if(tabrefmin[j]>=0 && tabrefmin[j]<vfc2[i].y)
	    tabheure[tabrefmin[j]]=heure_t(0,tabmin[j]);
	}
      }
      
      //m=M=0;
      for(j=0;j<vfc2[i].y;j++) {
	tabid[j]=readint(fd,2);
	//if(j==0) m=M=tabid[j];
	//if(m>tabid[j]) m=tabid[j];
	//if(M<tabid[j]) M=tabid[j];
      }
      //if(vfc2[i].y) _debug2("YA: %d ... %d\n",m,M);
      
      for(j=0;j<vfc2[i].y;j++) {
	tabmark[j]=readint(fd,1);
	//if(j<10) _debug2("%d ",tabmark[j]);
      }
      //if(vfc2[i].y) _debug2("\n");
      
#if 0
      for(j=0;j<2*vfc2[i].y;j++) {
	tabyc[j]=readint(fd,2);
      }
#endif
      
      _debug2("-- F --\n");
      
      int k=0;
      int n=0;
      while(k<vfc2[i].y) {
	int ii=k;
	
	//_debug2("(%d) train\n",k);
	
	train_t *t=new train_t();
	
	heure_t he=tabheure[k+1];
	
	//_debug2("periode %d\n",tabid[k]);
	per_t *p=db.per_[tabid[k]];
	if(p==NULL) _warn("periode %d non trouvée\n",tabid[k]);
	t->setper(p);
	//t->setpid(tabid[k]);
	  
	try {
	  
	  int tid_=readint(fd,4);
	  _debug2("id %d\n",tid_);
	  k++;
	  
	  if(tid_!=0) {
	    train_t *inst=NULL;
	    if(db.train_[tid_]) {
	      //_debug2("deja dans FIC1\n");
	      inst=db.train_[tid_];
	    }
	    else {
	      //_debug2("pas dans FIC1\n");
	      db.train_[tid_]=t;
	    }
	    //if(train2b_[tid_]) _debug2("deja dans fic2\n");
	    //train2b_[tid_]++;
	      
	      
	    gare_t *b=db.gares->_gare[db.gareid_[tabid[k]]];
	    if(b==NULL) {_warn(" gare %d non trouvée\n",tabid[k]);}
	    else _debug2(" gare %d %s\n",tabid[k], b->name.c_str());
	    t->adddess(dess_t(he,0,b->getid()));
	    _debug2(" %s\n",he.str().c_str());
	      
	      
	    int a21=readint(fd,2); //services ?
	    int a22=readint(fd,2);
	    k++;
	    
	    
	    int a3,a4;
	    while(k< vfc2[i].y && tabmark[k]==0) {
	      a3=readint(fd,2);
	      a4=readint(fd,2);

	      a3=a3%1440; // il y a des choses bizares dans ce fichier...	
	      a4=a4%1440; 
	      
	      gare_t *b=db.gares->_gare[db.gareid_[tabid[k]]];
	      if(b==NULL) {_warn(" gare %d non trouvée\n",tabid[k]);}
	      else _debug2(" gare %d %s\n",tabid[k], b->name.c_str());
		
	      he+=a4;
	      t->adddess(dess_t(he,a3,b->getid()));
	      he+=a3;
	      
	      _debug2(" %s %d %d\n",he.str().c_str(),a4,a3);
	      
	      k++;
	    } 
	    
	    if(inst) {
	      printf("ajoute:\n");
	      db.printtrain(t);
	      printf("dans:\n");
	      db.printtrain(inst);
	      printf("----\n");
	      inst->insert(t,db.gareid_[vfc2[i].b],db.gareid_[vfc2[i].c]);
	    }
	  }
	} catch (...) {
	  break;
	}
	
	n++;
      }
      
      for(j=0;j<vfc2[i].z;j++) {
	tabza[j][0]=readint(fd,4);
	tabza[j][1]=readint(fd,4);
      }
      
      for(j=0;j<vfc2[i].z;j++) {
	if(j>5 && j<vfc2[i].z-5) j=vfc2[i].z-5;
	_debug2("ZA[%d]: %d %d\n",j,tabza[j][0],tabza[j][1]);
      }
      
    }
    
    close(fd);
    
  }
#endif
  
  _debug("--trains 3--\n");
  
  {
    
    unsigned char bu[32];
    int fd=open("TRANCHES",O_RDONLY);
    
    while(1) {
      try {
	int d=0;
	readchars(fd,5,bu);
	bu[5]=0;
	int i_=atoi((char*)bu);
	//_debug("train %d\n",i_);
	train_t *t=db.train_[i_];
	skip(fd,1);d+=6;
	
	readchars(fd,4,bu);bu[4]=0;
	int r_=atoi((char*)bu); //regime ?
	const per_t *tp=t?t->getper():NULL;
	const per_t *tp_=db.per_[r_];
	skip(fd,1);d+=5;
	
	readchars(fd,7,bu);bu[7]=0;d+=7;
	string sid((char*)bu);
	if (t) {
	  int idt=db.newidtrain();
	  db._train[idt]=t;
	  db._idtrain[sid]=idt;
	  t->setsid(sid);
	}
	
	if(t==NULL) {
	  _debug2("train %d '%s' non trouvé\n",i_,sid.c_str());
	}

#if 0 //OSF c'est toujours l'union des periodes...
	if(t&&r_&&tp&&tp_&&!(*tp==*tp_)) {
	  _warn("train %d '%s': periodes contradictoires %d %d!!!\n",i_,sid.c_str(),r_,t->pid());
	  printf("%d:",t->pid());tp->print();
	  printf("%d:",r_);tp_->print();
	}
#endif

	//todo lien pour texte ???
	
	skip(fd,130-d); //todo aliases...
      } catch (...) {
	break;
      }
    }
    
    close(fd);

  }
  
  {
    dbs.types['1']="TEE";
    dbs.types['2']="IC";
    dbs.types['3']="TEOZ";
    dbs.types['6']="NAV";
    dbs.types['7']="BAN";
    dbs.types['8']="CAR";
    dbs.types['9']="BUS";
    dbs.types['D']="TGV Lyria";
    dbs.types['E']="EC";
    dbs.types['F']="TGV";
    dbs.types['G']="TGV";
    dbs.types['H']="TER";
    dbs.types['I']="Train GL";
    dbs.types['J']="Corail";
    dbs.types['K']="Eurostar";
    dbs.types['L']="Thalys";
    dbs.types['M']="TGNUIT";
    dbs.types['N']="Lunea";
    dbs.types['R']="ICE";
    dbs.types['S']="TGV";
    dbs.types['X']="TER";
    dbs.types['Y']="BAT";
    dbs.types['Z']="AVI";
  }
  return true;
}
  
string replacehome(const string &s)
{
  char *h=getenv("HOME");
  if(h==NULL) return s;
  int f=s.find("~");
  if(f<0) return s;
  string r=s;
  r.replace(f,1,h);
  return r;
}

bool load(dbs_t &dbs,const string &s) 
{
  string w=replacehome(s);
  _log("== lit données dans '%s' ==\n",w.c_str());
  char od[1000];
  getcwd(od,1000);
  chdir(w.c_str());
  
  db_t *a=new db_t(dbs.gares);
  bool r=load(dbs,*a,s);
  if(r) dbs.add(*a);
  chdir(od);
  return r;
}

static list<string> dbspaths;

bool load(dbs_t &dbs) {
  if(dbspaths.size()==0) return load(dbs,"");
  list<string>::const_iterator it;
  bool r=false;
  for(it=dbspaths.begin();it!=dbspaths.end();++it) {
    bool r2=load(dbs,*it);
    r=r||r2;
  }
  return r;
}

string basedir="";
string guiglade="";
  
bool loadconfig(const string &s)
{
  string w=replacehome(s);
  //_debug("essaye de lire '%s'\n",w.c_str());
  FILE* in=fopen(w.c_str(),"r");
  if(in==NULL) return false;
  char b[1000];
  while(fgets(b,1000,in)) {
    char ba[100],bb[500];
    sscanf(b,"%30s",bb); 
    if(b[0]=='#' || (b[0]=='/' && b[1]=='/')) continue;
    if(0==strcmp(bb,"BASE")) {
      sscanf(b,"%20s %500s",bb,ba);
      if(strlen(ba)>0)
	basedir=string(ba);
      continue;
    }
    if(0==strcmp(bb,"GUI")) {
      sscanf(b,"%20s %500s",bb,ba);
      if(strlen(ba)>0)
	guiglade=string(ba);
      continue;
    }
    if(0==strcmp(bb,"RIHO")) {
      sscanf(b,"%20s %500s",bb,ba);
      if(strlen(ba)>0) dbspaths.push_back(ba);
      continue;
    }
    if(b[0]=='/' || b[0]=='~') {
      int i=strlen(b)-1;
      while(i>=0 && b[i]=='\n' || b[i]==' ' || b[i]=='\t') i--;
      i++;b[i]=0;
      if(strlen(b)>0) dbspaths.push_back(b);
    }
  }
  _log("lecture du fichier de configuration: %s\n",s.c_str());
  fclose(in);
  return true;
}

bool loadconfig()
{
  if(loadconfig("~/.horails/horails.conf")) return true;
  if(loadconfig("~/.horails.conf")) return true;
  if(loadconfig("/etc/horails.conf")) return true;
  if(loadconfig("/etc/horails/horails.conf")) return true;
  _warn("erreur à la lecture du fichier de configiration\n");
  return false;
}
