#ifndef _UTILS_HPP_
#define _UTILS_HPP_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <map>
#include <set>
#include <list>
#include <string>
#include <vector>
#include <queue>
using namespace std;

/* -> log.h */
extern int loglevel;

#define _log_(a,...) {if(a<=loglevel) fprintf(stderr, __VA_ARGS__);}
#define _error(...) _log_(0,__VA_ARGS__)
#define _warn(...) _log_(1,__VA_ARGS__)
#define _log(...) _log_(2,__VA_ARGS__)
#define _info(...) _log_(3,__VA_ARGS__)
#define _debug(...) _log_(4,__VA_ARGS__)
#define _debug2(...) _log_(5,__VA_ARGS__)



template <class T_t> void xchg(T_t &a,T_t &b) { T_t t=a;a=b;b=t; }

inline string stripspaces(const string &s)
{
  int i=s.size()-1;
  while(i>0 && (s[i]==' ' || s[i] =='\n' || s[i]=='\t')) i--;
  i++;
  
  return s.substr(0,i);
}

inline string capital(const string &s)
{
  char b[s.size()];
  for(int i=0;i<=s.size();i++) {
    b[i]=s[i];
    if(b[i]>='a' && b[i]<='z') b[i]+='A'-'a';
  }
  return b;
}

inline int toint(const string &s) 
{
  return atoi(s.c_str());
}

class exception_t {
public:
  exception_t():errn(-1) {}
  exception_t(int i):errn(i) {}
  exception_t(const string &s):errn(-1),error(s) {}
  int errn;
  string error;
};



#if 0
inline void printbin(int j)
{
  for(int i=1;i<256;i*=2)
    printf("%c",(j&i)?'1':'0');
  printf(" ");
}

inline void printbin2(int j)
{
  for(int i=128;i>0;i/=2)
    printf("%c",(j&i)?'1':'0');
  printf(" ");
}

inline void printbin(unsigned long long i, int n)
{
  for(int j=0;j<n;j++) {
    printbin(i&255);
    i/=256;
  }
}

inline void printbin2(unsigned long long i, int n)
{
  for(int j=0;j<n;j++) {
    printbin2((i>>(8*(n-1-j)))&255);
    //i/=256;
  }
}
#endif

extern unsigned long long dec;

inline string readstring(int fd,int i)
{
  char s[100];
  int r,r2=0;

  while(r2!=i) {
    r=read(fd,s+r2,i-r2);
    if(r<=0) throw int(0);
    r2+=r;
    dec+=r;
  }
  s[i]=0;
  return s;
}

inline void readchars(int fd,int i,unsigned char *s)
{
  int r,r2=0;
  
  while(r2!=i) {
    r=read(fd,s+r2,i-r2);
    if(r<=0) throw int(0);
    r2+=r;
    dec+=r;
  }
  //s[i]=0;
}

inline unsigned long long readint(int fd,int i)
{
  unsigned char s[100];
  int r,r2=0;

  while(r2!=i) {
    r=read(fd,s+r2,i-r2);
    if(r<=0) throw int(0);
    r2+=r;
    dec+=r;
  }

  unsigned long long a=0,d=1;

  for(int j=0;j<i;j++) {
    a+=d*s[j];
    d*=256;
  }
  return a;
}


inline void skip(int fd, unsigned long long ski)
{
  char s[1000];
  while(ski>0) {
    unsigned long long si=ski;
    if(si>1000) si=1000;
    int r=read(fd,s,si);
    if(r==0)  {
      printf("erreur EOF\n");
      throw int(1);
    }
    //printf("skip %d\n",r);
    if(r==-1) {
      printf("erreur skip\n");
      throw int(1);
    }
    dec+=r;
    ski-=r;
  }
}

#if 0
inline void skipaff(int fd, unsigned long long ski)
{
  unsigned char s[1000];
  while(ski>0) {
    unsigned long long si=ski;
    if(si>1) si=1;
    int r=read(fd,s,1);
    //printf("skip %d\n",r);
    if(r==-1) {
      printf("erreur skip\n");
      throw int(1);
    }
    printf("%d ",(unsigned int)s[0]);
    dec+=r;
    ski-=r;
  }
  printf("\n");
}
#endif

#endif
