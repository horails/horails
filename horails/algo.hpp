#ifndef _ALGO_HPP_
#define _ALGO_HPP_

#include "dateheure.hpp"
#include "utils.hpp"
#include "heap.hpp"
#include "db.hpp"
#include "load.hpp"

#define SUPER
#define SUPER2

class precomp_t {

protected:

  const dbs_t &dbs;

  precomp_t(const dbs_t &d):dbs(d) {pretraite();}

  struct td_t {
    td_t(const train_t *tt=NULL, int ndd=0):t(tt),nd(ndd) {}
    const train_t *t;
    int nd;
  };

  class ag_t {
  public:
    void addtrain(const db_t *db,const train_t *t,int i) {td[db].push_back(td_t(t,i));}
    map<const db_t*,list<td_t> > td;
  };
  
  map<int,ag_t> gare;


  struct tpsmin_t {

    tpsmin_t(int bb=0,int tt=0):g(bb),t(tt) {}
    tpsmin_t(const tpsmin_t &o):g(o.g),t(o.t) {}
    int g;int t;
  };

  map<int, list<tpsmin_t> > tempsming; // temps des gares pour arriver à id
  map<int,int> tempsmingg; 

public:

  void testtempsmin(int a,int b,int t) {
    //printf("testpsmin %d %d : %d\n",a,b,t);
    int i=a*MAXGARES+b;
    map<int,int>::iterator it=tempsmingg.find(i);
    if(it==tempsmingg.end() || it->second>t)
      tempsmingg[i]=t;
  }
  
  int tempsmin(int a,int b) const {
    // retourne le temps min entre a et b
    if(a==b) return 0;
    map<int,int>::const_iterator it=tempsmingg.find(a*MAXGARES+b);
    if(it==tempsmingg.end()) {
      return -1;
    }
    return it->second;
  }

  void maketempsmin() {
    map<int,int>::const_iterator it;
    for(it=tempsmingg.begin();it!=tempsmingg.end();++it) {
      int a=it->first/MAXGARES;
      int b=it->first%MAXGARES;
      tempsming[b].push_back(tpsmin_t(a,it->second));
      //printf("tpsmin %d %d : %d\n",a,b,it->second);
    }
  }

  void pretraite()
  {
    list<db_t*>::const_iterator it;
    for(it=dbs.dbs.begin();it!=dbs.dbs.end();++it) 
      pretraite(**it);

    map<int, map<int, int> >::const_iterator itm;
    for(itm=dbs.pass.begin();itm!=dbs.pass.end();++itm) {
      map<int,int>::const_iterator itn;
      for(itn=itm->second.begin();itn!=itm->second.end();++itn) 
	testtempsmin(itm->first,itn->first,itn->second);
    }
    
    maketempsmin();
  }
  
  void pretraite(const db_t &db)
  {
    map<int,train_t*>::const_iterator it;
    int i,j;
    for(it=db._train.begin();it!=db._train.end();++it) {
      for(i=0;i<it->second->size();i++)
	gare[(*(it->second))[i].gare].addtrain(&db,it->second,i);

      vector<int> vt;
      vt.resize(it->second->size());
      for(i=0;i<it->second->size();i++) {
	vt[i]=-((*(it->second))[i].arret);
	if(i) {
	  for(j=i-1;j>=0;j--) {
	    vt[j]+=((*(it->second))[i].arriv-(*(it->second))[i-1].arriv); 
	    //+(*(it->second))[i-1].arret;
	    testtempsmin((*(it->second))[j].gare,(*(it->second))[i].gare,vt[j]);
	  }
	}
      }
    }
  }


  // iterateurs

  class tdit_t {
  public:
    list<list<td_t>* > l;
    list<td_t>::const_iterator it;

    bool passok;
    map<int, map<int,int> >::const_iterator itpassm;
    map<int,int>::const_iterator itpass;
    mutable train_t train; //fakes pour passages
    per_t per;
    td_t td;
    dateheure_t dh;
    
    bool isend() const {return l.empty();}
    const td_t &operator*() const {
      if(!passok) {
	train[0].arriv=dh.heure()+(-itpass->second);
	train[0].gare=itpass->first;
	train[2].arriv=dh.heure()+itpass->second;
	train[2].gare=itpass->first;
	return td;
      }
      return *it;
    }
    const td_t *operator->() const {
      return &**this;
    }
    void inc() {
      if(!passok) {
	++itpass;
	if(itpass==itpassm->second.end()) {
	  passok=true;
	}
	return;
      }
      ++it;
      while(it==l.front()->end()) {
	l.pop_front();
	if(l.empty()) return;
	it=l.front()->begin();
      }
    }
  };
  
  tdit_t getg3(int g,const dateheure_t &dh) {
    tdit_t t;
    const db_t *db[3];
    
    db[0]=dbs.getdb(dh.date()-1);      
    db[1]=dbs.getdb(dh.date());
    db[2]=dbs.getdb(dh.date()+1);      
    
    if(db[0]) t.l.push_back(&(gare[g].td[db[0]]));
    if(db[1] && db[1]!=db[0]) t.l.push_back(&(gare[g].td[db[1]]));
    if(db[2] && db[2]!=db[1]) t.l.push_back(&(gare[g].td[db[2]]));
    
    if(!t.l.empty()) {
      t.it=t.l.front()->begin();
      while(t.it==t.l.front()->end()) { // au cas ou une liste est vide
	t.l.pop_front();
	if(t.l.empty()) break;
	t.it=t.l.front()->begin();
      }
    }
    
    t.itpassm=dbs.pass.find(g);
    if(t.itpassm==dbs.pass.end()) t.passok=true;
    else {
      t.itpass=t.itpassm->second.begin();
      if(t.itpass==t.itpassm->second.end()) {
	t.passok=true;
      } else {
	t.passok=false;
	t.train.setsid("Passage");
	t.train.setspecial();
	t.train.setper(&t.per);
	t.train.adddess(dess_t(dh.heure(),0,0));
	t.train.adddess(dess_t(dh.heure(),0,g));
	t.train.adddess(dess_t(dh.heure(),0,0));
	t.td.t=&t.train;
	t.td.nd=1;
	t.dh=dh;
      }
    }
    return t;
  }
};


struct trajet_part_t {
  trajet_part_t() {}
  trajet_part_t(const trajet_part_t &o):train(o.train),gdep(o.gdep),garr(o.garr),hdep(o.hdep),harr(o.harr) {}
  trajet_part_t(const train_t *t,int gd,int ga,const dateheure_t &hd,const dateheure_t &ha):
    train(t),gdep(gd),garr(ga),hdep(hd),harr(ha) {}
  const train_t *train; // id + db ?
  int gdep,garr;
  dateheure_t hdep,harr;
  void print(const gares_t &gares) const {
    if(train==NULL) {
      _log("autre: %s %s %s %s\n",gares.garestring(gdep).c_str(),gares.garestring(garr).c_str(),hdep.str().c_str(),harr.str().c_str());
    } else {
      _log("train %s: %s %s %s %s\n",train->getsid().c_str(),gares.garestring(gdep).c_str(),gares.garestring(garr).c_str(),hdep.str().c_str(),harr.str().c_str());
    }
  }
};

struct trajet_t {
  trajet_t() {}
  trajet_t(const trajet_t &o):l(o.l) {}
  void add(const train_t *t, int gd, int ga,const dateheure_t &hd,const dateheure_t &ha, int sens) {
    if(sens==1) {
      if(t==NULL || t->isspecial())
	l.push_back(trajet_part_t(NULL,gd,ga,hd,ha));
      else 
	l.push_back(trajet_part_t(t,gd,ga,hd,ha));
    } else {
      if(t==NULL || t->isspecial())
	l.push_front(trajet_part_t(NULL,gd,ga,hd,ha));
      else 
	l.push_front(trajet_part_t(t,gd,ga,hd,ha));
    }
  } 
  int size() const { return l.size(); }
  list<trajet_part_t> l;
  void print(const gares_t &gares) const {
    list<trajet_part_t>::const_iterator it;
    for(it=l.begin();it!=l.end();++it) {
      it->print(gares);
    }
  }
};

struct optionsalgo_t {
  int gd,ga;
  int tpscorr;
  int nbrcorr;
  vector<int> via;
  list<int> garesint;
  const dbs_t *dbs;

  void reverse() {
    xchg(gd,ga);
    vector<int> v;
    for(int i=0;i<v.size();i++) {
      v[i]=via[v.size()-i-1];
    }
    via=v;
  }
};

struct score_t {
  //score_t(): temps(0),temps_trajet(0),corr(0) {}
  score_t(const dateheure_t &dh):data(NULL),start(dh),start2(dh),end(dh),end2(dh),temps(0),temps_trajet(0),corr(0),via(0) {}
  score_t(const score_t &s):data(s.data),start(s.start),start2(s.start2),end(s.end),end2(s.end2),temps(s.temps),temps_trajet(s.temps_trajet),corr(s.corr),via(s.via) {}
  optionsalgo_t *data;
  dateheure_t start;
  dateheure_t start2;
  dateheure_t end;
  dateheure_t end2;
  int temps;
  int temps_trajet;
  int corr;
  int via;
  // type de trains
  //int temps_corr;
  void addtrajet_(const train_t *t, int gd, int ga,dateheure_t hd,dateheure_t ha,int sens) {
    if(sens<0) {xchg(gd,ga);xchg(hd,ha);}
    int tt=ha-hd;
    int tc;
    if(sens==1) {
      tc=hd-end;
      end2=end=ha;
      if(corr==0) {
	temps_trajet=tt;
	start2=hd;
      } else {
	temps_trajet+=tc+tt;
      }
    } else { //sens==-1
      tc=start-ha;
      start2=start=hd;
      if(corr==0) {
	temps_trajet=tt;
	end2=ha;
      } else {
	temps_trajet+=tc+tt;
      }
    }
    temps+=tc+tt;
    corr++;
    
    if(data && via < data->via.size() && data->dbs->issuper(data->via[via],ga))
      via++;

  }
  
  void print() const {
    printf("deb:%s, depart %s, arr:%s, fin:%s, corr=%d, temps=%s, trajet=%s",start.str().c_str(),start2.str().c_str(),end.str().c_str(),end2.str().c_str(),corr,heure_t(0,temps).str().c_str(),heure_t(0,temps_trajet).str().c_str());
  }
};

struct score2_t : public score_t, public trajet_t {
  //score2_t(): elm(NULL) {}
  score2_t(const score2_t &s):score_t(s),trajet_t(s),gare_dep(s.gare_dep),gare(s.gare),gare_prev(s.gare_prev),hd(s.hd),ha(s.ha)/*,trajet(s.trajet)*/,id(s.id),id_ref(s.id_ref),elm(s.elm) {}
  score2_t(const dateheure_t &dh,int g):score_t(dh),trajet_t(),gare_dep(g),gare(g),gare_prev(0),hd(dh),ha(dh),elm(NULL) {id=0;id_ref=0;}
  int gare_dep;
  int gare;
  int gare_prev;
  const train_t *train;
  dateheure_t hd,ha;
  int id,id_ref;
  static int id_;
  //trajet_t trajet;
  heap_t<score2_t*,float>::heap_elm_t *elm;
  
  void addtrajet(const train_t *t, int gd, int ga,dateheure_t hd,dateheure_t ha,int sens) {
    //if(sens<0) {xchg(gd,ga);xchg(hd,ha);}
    id_ref=id;
    newid();
    //trajet_t::add(t,gd,ga,hd,ha,sens);
    if(t==NULL || t->isspecial())
      train=NULL;
    else
      train=t;
    gare=ga;
    gare_prev=gd;
    this->hd=hd;
    this->ha=ha;
    score_t::addtrajet_(t,gd,ga,hd,ha,sens);      
  }
  void newid() {id=++id_;}
  void reverse() {
    xchg(gare_dep,gare);
  }
};


struct scores_t {
  ~scores_t() {
    list<score2_t*>::iterator it;
    for(it=l.begin();it!=l.end();++it)
      delete *it;
  }
  list<score2_t*> l;
  void add(score2_t *s) {l.push_back(s);}
};


class algo2_t : public precomp_t {
public:
  algo2_t(const dbs_t &dbs):precomp_t(dbs) {}

protected:
  int (*cmp)(const optionsalgo_t &data, const score_t &a, const score_t &b);
  optionsalgo_t *cmpdata;

  /*
    les scores forment un ordre partiel
    ils augmententent sctrictement si on ajoute un trajet
  */

  static int cmptemps(const optionsalgo_t &data, const score_t &a, const score_t &b)
  {
    if(a.temps<b.temps) return 1;
    if(b.temps<a.temps) return -1;
    if(a.temps_trajet<b.temps_trajet) return 1;
    if(b.temps_trajet<a.temps_trajet) return -1;
    return 0;
  }
  
  static int cmptemps2(const optionsalgo_t &data, const score_t &a, const score_t &b)
  {
    if(a.temps==b.temps && a.temps_trajet==b.temps_trajet) return 0;
    if(a.temps<=b.temps && a.temps_trajet<=b.temps_trajet) return 1;
    if(a.temps>=b.temps && a.temps_trajet>=b.temps_trajet) return -1;
    return 2; //incomparable
  }

#define MAX_COR 6

  static int cmptemps2corr(const optionsalgo_t &data, const score_t &a, const score_t &b)
  {
    int r= cmptemps2(data,a,b);
    if(a.corr==b.corr || (a.corr>MAX_COR && b.corr>MAX_COR)) {
      return r;
    }
    if(a.corr<b.corr) {
      if(r==1) return 1;
      if(r==0) return 1;
      return 2;
    }
    if(a.corr>b.corr) {
      if(r==-1) return -1;
      if(r==0) return -1;
      return 2;
    }
    return 2; //incomparable
  }

  static int cmptemps2corrvia(const optionsalgo_t &data, const score_t &a, const score_t &b)
  {
    int r=cmptemps2corr(data,a,b);
    if(a.via==b.via) return r;
#if 1
    if(a.via>b.via) {
      if(r==1) return 1;
      if(r==0) return 1;
      return 2;
    }
    if(a.via<b.via) {
      if(r==-1) return -1;
      if(r==0) return -1;
      return 2;
    }
#endif
    return 2; //incomparable
  }

  static bool testscore(const optionsalgo_t &data, const score_t &a)
  {
    if(data.nbrcorr && a.corr>data.nbrcorr) return false;
    return true;
  }


private:  

  struct algog_t: public scores_t {
    algog_t(const algog_t &o):scores_t(o),auth(o.auth),final(o.final) {}
    algog_t():scores_t(),auth(2),final(false) {}

    int auth;
    bool final;
    //void change(const dateheure_t &dhh, const trajet_t &t){ deja=true; dh=dhh;  trajet=t;}
  };

  score2_t maketrajet(const score2_t &s,const map<int,algog_t> &g,int sens)
  {
    printf("maketrajet\n");
    score2_t r(s);
    printf(" id=%d\n",s.id);
    if(s.id==0) return r;
    printf(" id_ref=%d\n",s.id_ref);
    if(s.id_ref) {
      printf(" gares=%d %d\n",s.gare,s.gare_prev);
      map<int,algog_t>::const_iterator it;
      it=g.find(s.gare_prev);
      if(it!=g.end()) {
	list<score2_t*>::const_iterator it2;
	for(it2=it->second.l.begin();it2!=it->second.l.end();++it2) {
	  if((*it2)->id==s.id_ref) {
	    score2_t v=maketrajet(**it2,g,sens);
	    r.l=v.l;
	    break;
	  }
	}
	if(it2==it->second.l.end()) {
	  _warn("chemin %d introuvable en gare %d\n",s.id_ref,s.gare_prev);
	}
      }
    }
    if(sens>0)
      r.trajet_t::add(s.train,s.gare_prev,s.gare,s.hd,s.ha,sens);
    else 
      r.trajet_t::add(s.train,s.gare,s.gare_prev,s.ha,s.hd,sens);
    return r;
  }

  void remove(heap_t<score2_t*,float> &q,score2_t &s) {
    if(s.elm) {
      q.remove(s.elm);
      s.elm=NULL;
    }
  }

  void removesup(heap_t<score2_t*,float> &q,scores_t &ss, const score_t &s) {
    list<score2_t*>::iterator it=ss.l.begin(),it2;
    while(it!=ss.l.end()) {
      it2=it;++it2;
      if(cmp(*cmpdata,s,**it)==1) {
	remove(q,**it);
	delete *it;
	ss.l.erase(it);
      }
      it=it2;
    }
  }

  bool scoresup(const score_t &s,const scores_t &ss, bool ttpsmin,int gd=0,int ga=0) {
    score_t st(s);
    int ntps=0;
    int ncorr=0;
    if(ttpsmin && gd!=ga) {
      ncorr++;
      ntps=tpsmin[gd];
      if(tempsmin(gd,ga)<0) ncorr++;
    }
    list<score2_t*>::const_iterator it=ss.l.begin();
    while(it!=ss.l.end()) {
      score_t ss(s);
      ss.temps_trajet+=ntps;
      ss.temps+=ntps;
      ss.corr+=ncorr;
      ss.end+=ntps;
      int r=cmp(*cmpdata,**it,ss);
      if(r==1 || r==0) {
	return true;
      }
      ++it;
    }
    return false;
  }

  map<int,int> tpsmin; //tempsmin[i] c'est le temps min pour aller de la gare i à arrivée
  // calculée par un BFS simple

  struct tm_t : public tpsmin_t {
    tm_t():elm(NULL) {}
    tm_t(const tm_t &o):tpsmin_t(o),elm(o.elm) {}
    tm_t(const tpsmin_t &o):tpsmin_t(o),elm(NULL) {}
    heap_t<tm_t*,int>::heap_elm_t *elm;
  };

  struct algogt_t {
    algogt_t(tm_t *tt=NULL):tm(tt),auth(2),ok(false) {}
    tm_t *tm;
    int auth;
    bool ok;
  };
  
  void precomptempsmin(int garr,int tcorr, const list<int> &garesint) {
    tpsmin.clear();

    map<int,algogt_t> gg;
    heap_t<tm_t*,int> q;

    list<int>::const_iterator itgi;
    for(itgi=garesint.begin();itgi!=garesint.end();++itgi) {
      gg[*itgi].auth=1;
#ifdef SUPER2
      map<int,list<int> >::const_iterator itpz;
      list<int>::const_iterator itp;
      itpz=dbs.super.find(*itgi);
      if(itpz!=dbs.super.end()) {
	for(itp=itpz->second.begin();itp!=itpz->second.end();++itp) {
	  gg[*itp].auth=1;
	}
      }
#endif
    }
    
    tm_t *e=gg[garr].tm=new tm_t(tpsmin_t(garr,0));
    e->elm=q.push(e,0);

#ifdef SUPER2
    map<int,list<int> >::const_iterator itpz;
    list<int>::const_iterator itp;
    itpz=dbs.super.find(garr);
    if(itpz!=dbs.super.end()) {
      for(itp=itpz->second.begin();itp!=itpz->second.end();++itp) {
	e=gg[*itp].tm=new tm_t(tpsmin_t(*itp,0));
	e->elm=q.push(e,0);
      }
    }
#endif
    
    while(!q.empty()) {
      tm_t *e=q.pop();
      e->elm=NULL;
      gg[e->g].tm=NULL;
      gg[e->g].ok=true;
      
      //printf("tps min %d: %d\n",e->g,e->t);

      if(gg[e->g].auth>=2) {
	map<int,int>::iterator itm=tpsmin.find(e->g);
	if(itm==tpsmin.end() || itm->second>e->t)
	  tpsmin[e->g]=e->t;
	
	list<tpsmin_t>::const_iterator it;
	for(it=tempsming[e->g].begin();it!=tempsming[e->g].end();++it) {
	  int g2=it->g;
	  tm_t *e2=gg[g2].tm;
	  int t2=e->t+tcorr+it->t;
	  //printf(" %d: %d\n",g2,t2);
	  if(e2) {
	    if(e2->elm && e2->t>t2) {
	      e2->t=t2;
	      q.dec(e2->elm,t2);
	    }
	  } else 
	    if(gg[g2].ok==false) {
	      e2=gg[g2].tm=new tm_t(tpsmin_t(g2,t2));
	      e2->elm=q.push(e2,t2);
	    }
	}
      }
      delete e;
    }
    
  } 
  
  // heuristique pour algo
  float heur(int gdep,int garr,const score_t &s) {
    return s.temps+tpsmin[gdep];
  }

  
public:
  
  
  
  //list<score2_t> algomoinscon(int dep, int arr,const dateheure_t &d,int timecorr, int sens=1, const list<int> &garesint=list<int>(), const vector<int> &via=vector<int>()) {
  list<score2_t> algomoinscon(optionsalgo_t ddata,const dateheure_t &d, int sens=1) {
    int nbrp=0,nbrt=0,nbrt2=0,nbrd=0;

    if(sens==-1) ddata.reverse();
    
    int dep=ddata.gd; int arr=ddata.ga;

    //optionsalgo_t ddata;
    cmp=cmptemps2corrvia; //cmptemps2corr;
    ddata.dbs=&dbs;
    //ddata.via=ddata.via;
    cmpdata=&ddata;
    
    precomptempsmin(arr,ddata.tpscorr,ddata.garesint);

    printf("tpsmin bfs %d -> %d: %d\n",dep,arr,tpsmin[dep]);
    
    map<int,algog_t> gares;       
    heap_t<score2_t*,float> q;
    list<score2_t> lr;

    gares[arr].final=true;

    list<int>::const_iterator itgi;
    for(itgi=ddata.garesint.begin();itgi!=ddata.garesint.end();++itgi) {
      gares[*itgi].auth=1;
#ifdef SUPER2
      map<int,list<int> >::const_iterator itpz;
      list<int>::const_iterator itp;
      itpz=dbs.super.find(*itgi);
      if(itpz!=dbs.super.end()) {
	for(itp=itpz->second.begin();itp!=itpz->second.end();++itp) {
	  gares[*itp].auth=1;
	}
      }
#endif
    }

    map<int,list<int> >::const_iterator itpz;
    list<int>::const_iterator itp;
#ifdef SUPER2
    itpz=dbs.super.find(arr);
    if(itpz!=dbs.super.end()) {
      for(itp=itpz->second.begin();itp!=itpz->second.end();++itp) 
	gares[*itp].final=true;
    }
#endif

    score2_t *s=new score2_t(d,dep);
    s->data=&ddata;
    gares[dep].add(s);
    s->elm=q.push(s,heur(dep,arr,*s));
    
#ifdef SUPER
    itpz=dbs.super.find(dep);
    if(itpz!=dbs.super.end()) {
      for(itp=itpz->second.begin();itp!=itpz->second.end();++itp) {
	s=new score2_t(d,*itp);
	s->data=&ddata;
	gares[*itp].add(s);
	s->elm=q.push(s,heur(*itp,arr,*s));
      }
    }
#endif

    
    while(!q.empty()) {
      // prende le g/s qui minimise l'heuristique
      nbrp++;
      score2_t *e=q.pop();
      int g=e->gare;
      e->elm=NULL;

      printf("gare %d\n",g);
      
      // pour tous les voisins g2 de g
      dateheure_t dh;
      if(sens>0) 
	dh=e->end+(e->corr?ddata.tpscorr:0);
      else 
	dh=e->start+(e->corr?-ddata.tpscorr:0);
      
      tdit_t it=getg3(g,dh);
      
      //FIX!!!! heures d'ete/hiver!!!
      
      // pour tous les trains..
      for(;!it.isend();it.inc()) {
	nbrt++;
	const train_t *t=it->t;	
	//_debug("((train %s: (%d/%d)))\n",t->getsid().c_str(),it->nd,t->size());
	if(sens==1 && it->nd>=t->size()) continue; //terminus
	if(sens==-1 && it->nd==0) continue; //anti-terminus
	
	//FIX: ATTENTION aux heures >=24 !!! ok???
	
	int d=0;
	dateheure_t dht;
	dateheure_t dhm=dh+(sens*24*60);
	for(d=((sens>0)?0:2);d<3&&d>=0;d+=sens) {
	  dht=(dh.date()-1+d);
	  dht.addheure((*t)[it->nd].arriv+(*t)[it->nd].arret);
	  if(sens==1 && dht >= dh && dht < dhm) break;
	  if(sens==-1 && dh >= dht && dhm < dht) break;
	}
	
	bool tt =d<3 && t->test(dh.date()-1+d);
	
	if(tt) {
	  nbrt2++;
	  bool fintrain=false;
	  
	  for(int j=it->nd+sens;fintrain==false && j>=0 && j<t->size();j+=sens) {
	    nbrd++;
	    
	    int g2=(*t)[j].gare;
	    int g2a=g2;
	    bool f=gares[g2].final;
	    if(f) g2a=arr;
	    
	    // si pas "grande" gare -> on fait pas de correspondance
	    if(!f && dbs.gares.getlevel(g2)==0) { 
	      continue;
	    }

	    if(gares[g2].auth<2) { //gare interdite
	      continue;
	    }
	    
	    dateheure_t dh2=(dh.date()-1+d);
	    dh2.addheure((*t)[j].arriv);
	    if(sens<0) dh2+=(*t)[j].arret;

	    //  calcule le nouveau score s2 à partir de l'ancien + train
	    score2_t *s2=new score2_t(*e);
	    s2->addtrajet(it->t,g,(*t)[j].gare,dht,dh2,sens);
	    
	    if(testscore(ddata,*s2)==false) {
	      delete s2;
	      continue;
	    }
	    
	    //s2->score_t::print();printf("\n");

	    if(scoresup(*s2,gares[g2a],false)) {
	      delete s2;
	      continue;
	    }
	    
	    // si s2 >= à un score dans scores_arr, continue
	    if(!f && scoresup(*s2,gares[arr],true,g2,arr)) {
	      delete s2;
	      // stoper le train là... ???
	      //fintrain=true;
	      continue;
	    }
	    
	    removesup(q,gares[g2a],*s2); 
	    gares[g2a].add(s2);
	    if(!f) {
	      s2->elm=q.push(s2,heur(g2,arr,*s2));
	    } else {
	      //on va pas plus loin...
	      break;
	    }
	  }
	}
      }
    }

    list<score2_t*>::iterator it;
    for(it=gares[arr].l.begin();it!=gares[arr].l.end();++it) {
      if((**it).via==ddata.via.size()) {
	score2_t la=maketrajet(**it,gares,sens);
	if(sens==-1) la.reverse();
	lr.push_back(la);
      }
    }
    
    printf("stats algo2: %d pop, trains: %d/%d, dess: %d\n",nbrp,nbrt2,nbrt,nbrd);

    return lr;
  }  


  
};

#endif
