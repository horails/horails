#include <gtk/gtk.h>

#include "dateheure.hpp"
#include "utils.hpp"
#include "heap.hpp"
#include "db.hpp"
#include "load.hpp"
#include "algo.hpp"

#include <sys/time.h>

#include "gui.hpp"

GtkWidget *fromfield,*tofield;
GtkWidget *jour,*mois,*annee,*heure,*minute;
GtkTreeStore *store;

dbs_t *dbs=NULL;
gares_t *gares=NULL;
algo2_t *algo=NULL;

static void changefieldgare( GtkWidget *widget,
			     gpointer   data )
{
  GtkComboBoxEntry *w=(GtkComboBoxEntry *)data;
  //GtkTreeModel* model=gtk_combo_box_get_model(w);  
  //gtk_tree_store_clear(store);
  
  g_print("recherche: %s\n",gtk_entry_get_text((GtkEntry*)widget));
  if(strlen(gtk_entry_get_text((GtkEntry*)widget))<2) return;
  
  list<string> lg=gares->searchstr(string(gtk_entry_get_text((GtkEntry*)widget)));
  list<string>::iterator it;
  int i=0;
  for(i=0;i<=30;i++) {
    gtk_combo_box_remove_text ((GtkComboBox*)w,0); //beurk comme GTK
  }
  for(it=lg.begin();it!=lg.end();++it) {
    gtk_combo_box_append_text ((GtkComboBox*)w,it->c_str());
  }
  //gtk_combo_box_popup ((GtkComboBox*)w);
  //gtk_combo_box_set_active((GtkComboBox*)w,0);
}


GtkWidget *widget_field_gare()
{
  GtkComboBoxEntry *w=(GtkComboBoxEntry*)gtk_combo_box_entry_new_text();
  g_signal_connect (G_OBJECT (gtk_bin_get_child((GtkBin*)w)), "changed",
		    G_CALLBACK (changefieldgare), w);
  return (GtkWidget *)w;
}

static void search_path( GtkWidget *widget,
                   gpointer   data )
{
  //g_print("%s\n",gtk_entry_get_text((GtkEntry*)gtk_bin_get_child((GtkBin*)fromfield)));

  int j=atoi(gtk_entry_get_text((GtkEntry*)jour));
  int m=atoi(gtk_entry_get_text((GtkEntry*)mois));
  int a=atoi(gtk_entry_get_text((GtkEntry*)annee));
  int h=atoi(gtk_entry_get_text((GtkEntry*)heure));
  int mi=atoi(gtk_entry_get_text((GtkEntry*)minute));;

  dateheure_t dh(date_t(a,m,j),heure_t(h,mi));
  
  int gd=gares->searchidbest(string(gtk_entry_get_text((GtkEntry*)gtk_bin_get_child((GtkBin*)fromfield))));
  int ga=gares->searchidbest(string(gtk_entry_get_text((GtkEntry*)gtk_bin_get_child((GtkBin*)tofield))));
  
  _debug("%d->%d\n",gd,ga);

  if(gd==0 || ga==0) {
    return;
  }

  printf("tpsmin %d %d: %d\n",gd,ga,algo->tempsmin(gd,ga));
  
  list<score2_t> lr=algo->algomoinscon(gd,ga,dh);
  trajets2store(*dbs,lr,store);
}

static gboolean delete_event( GtkWidget *widget,
                              GdkEvent  *event,
                              gpointer   data )
{
    /* If you return FALSE in the "delete_event" signal handler,
     * GTK will emit the "destroy" signal. Returning TRUE means
     * you don't want the window to be destroyed.
     * This is useful for popping up 'are you sure you want to quit?'
     * type dialogs. */

    //g_print ("delete event occurred\n");

    /* Change TRUE to FALSE and the main window will be destroyed with
     * a "delete_event". */

    return FALSE;
}

/* Another callback */
static void destroy( GtkWidget *widget,
                     gpointer   data )
{
    gtk_main_quit ();
}

const char *myitoa(int i)
{
  static char r[100];
  snprintf(r,100,"%d",i);
  return r;
}

int main( int   argc,
          char *argv[] )
{
  loadconfig();

  dbs_t db;
  load(db);
  dbs=&db;
  gares=&db.gares;
  
  algo2_t aa2(db);
  //aa2.pretraite(db);  
  algo=&aa2;
  
  GtkWidget *window;
  GtkWidget *button;

  struct timeval tv;
  struct timezone tz;

  gettimeofday(&tv,&tz);
  time_t t=tv.tv_sec-tz.tz_minuteswest*60;
  date_t nd(1970,1,1);
  heure_t nh(0,0);
  nd+=(t/60)/1440;
  nh+=(t/60)%1440;
  dateheure_t now(nd,nh);

  gtk_init (&argc, &argv);
  
  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  
  g_signal_connect (G_OBJECT (window), "delete_event",
		    G_CALLBACK (delete_event), NULL);
  
  g_signal_connect (G_OBJECT (window), "destroy",
		    G_CALLBACK (destroy), NULL);
  
  gtk_container_set_border_width (GTK_CONTAINER (window), 10);
  
  GtkWidget *vbox= gtk_vbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (window), vbox);
  gtk_widget_show (vbox);
  
  GtkWidget *hbox= gtk_hbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (vbox), hbox, TRUE, TRUE, 0);
  gtk_widget_show (hbox);
  
  GtkWidget *wl;
  wl=gtk_label_new ("De:");gtk_widget_show (wl);
  gtk_box_pack_start (GTK_BOX (hbox), wl, TRUE, TRUE, 0);
  fromfield = widget_field_gare(); //gtk_entry_new();
  gtk_box_pack_start (GTK_BOX (hbox), fromfield, TRUE, TRUE, 0);
  
  wl=gtk_label_new ("A:");gtk_widget_show (wl);
  gtk_box_pack_start (GTK_BOX (hbox), wl, TRUE, TRUE, 0);
  tofield = widget_field_gare(); //gtk_entry_new();
  gtk_box_pack_start (GTK_BOX (hbox), tofield, TRUE, TRUE, 0);
  gtk_widget_show (fromfield);
  gtk_widget_show (tofield);
  
  GtkWidget *hbox2= gtk_hbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (vbox), hbox2, TRUE, TRUE, 0);
  gtk_widget_show (hbox2);
  gtk_box_pack_start (GTK_BOX (hbox), gtk_label_new ("De:"), TRUE, TRUE, 0);
  
  wl=gtk_label_new ("Date:");gtk_widget_show (wl);
  gtk_box_pack_start (GTK_BOX (hbox2), wl, TRUE, TRUE, 0);
  
  jour = gtk_entry_new();
  gtk_box_pack_start (GTK_BOX (hbox2), jour, TRUE, TRUE, 0);
  gtk_widget_show (jour);
  gtk_entry_set_text((GtkEntry*)jour,myitoa(now.date().d));
  mois = gtk_entry_new();
  gtk_box_pack_start (GTK_BOX (hbox2), mois, TRUE, TRUE, 0);
  gtk_widget_show (mois);
  gtk_entry_set_text((GtkEntry*)mois,myitoa(now.date().m));
  annee = gtk_entry_new();
  gtk_box_pack_start (GTK_BOX (hbox2), annee, TRUE, TRUE, 0);
  gtk_widget_show (annee);
  gtk_entry_set_text((GtkEntry*)annee,myitoa(now.date().y));
  
  wl=gtk_label_new ("Heure:");gtk_widget_show (wl);
  gtk_box_pack_start (GTK_BOX (hbox2), wl, TRUE, TRUE, 0);
  
  heure = gtk_entry_new();
  gtk_box_pack_start (GTK_BOX (hbox2), heure, TRUE, TRUE, 0);
  gtk_widget_show (heure);
  gtk_entry_set_text((GtkEntry*)heure,myitoa(now.heure().h));
  minute = gtk_entry_new();
  gtk_box_pack_start (GTK_BOX (hbox2), minute, TRUE, TRUE, 0);
  gtk_widget_show (minute);
  gtk_entry_set_text((GtkEntry*)minute,myitoa(now.heure().m));
  
  
  button = gtk_button_new_with_label ("Cherche !");
  gtk_box_pack_start (GTK_BOX (vbox), button, TRUE, TRUE, 0);
  gtk_widget_show (button);
  
  GtkWidget *tree = gtk_tree_view_new();
  setup_tree(tree,store);
  gtk_widget_show (tree);
  
  gtk_box_pack_start (GTK_BOX (vbox), tree, TRUE, TRUE, 0);
  
  g_signal_connect (G_OBJECT (button), "clicked",
		    G_CALLBACK (search_path), NULL);
  
  //g_signal_connect_swapped (G_OBJECT (button), "clicked",
  //			      G_CALLBACK (gtk_widget_destroy),
  //                          G_OBJECT (window));
  
  //gtk_container_add (GTK_CONTAINER (window), button);
  
  gtk_widget_show (window);
  
  gtk_main ();
  
  return 0;
}
