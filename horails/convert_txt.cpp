#include "dateheure.hpp"
#include "utils.hpp"
#include "heap.hpp"
#include "db.hpp"
#include "load.hpp"
#include "algo.hpp"
#include "export_txt.hpp"

int main( int   argc,
          char *argv[] )
{
  loadconfig();

  dbs_t db;
  load(db);


  chdir("/home/rao/sncf/testexport/");
  export_txt(db);

  return 0;
}
