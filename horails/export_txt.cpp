#include "export_txt.hpp"
#include "utils.hpp"

void export_txt(const dbs_t &dbs) 
{

  if(dbs.dbs.size()!=1) {
    _warn("export_txt: db doit contenir une et une seule base !\n");
    return;
  }

  db_t &db(**dbs.dbs.begin());
  
  FILE *out=fopen("gares.txt","w");
  if(out==NULL) {
    _warn("export_txt: ne peut pas creer le fichier\n");
    return;
  }

  
  map<int,gare_t*>::const_iterator itg; //map sur id pour dess
  for(itg=dbs.gares._gare.begin();itg!=dbs.gares._gare.end();++itg) {
    fprintf(out,"%04d %s\n",itg->first,itg->second->name.c_str());
  }
  
  fclose(out);


  map<const per_t*,int> mapper;

  out=fopen("periodes.txt","w");
  if(out==NULL) {
    _warn("export_txt: ne peut pas creer le fichier\n");
    return;
  }

  fprintf(out,"# du %s au %s\n",db.dbstart.str().c_str(),(db.dbend-1).str().c_str());

  set<per_t*>::const_iterator itp;
  int i=1;
  for(itp=db.per.begin();itp!=db.per.end();++itp) {
    mapper[*itp]=i;
    fprintf(out,"%04d ",i);
    for(int j=0;j<db.dbnbrjours;j++)
      if((*itp)->test(db.dbstart+j)) fprintf(out,"1"); else fprintf(out,"0");
    fprintf(out,"\n");
    i++;
  }
  
  fclose(out);


  out=fopen("trains.txt","w");
  if(out==NULL) {
    _warn("export_txt: ne peut pas creer le fichier\n");
    return;
  }

  fprintf(out,"# <numero train> <numero periode> (<numero gare>, <heure d'arrivee>, <temps arret>) ...\n");

  map<int,train_t*>::const_iterator itt;
  for(itt=db._train.begin();itt!=db._train.end();++itt) {
    string nom=itt->second->getsid();
    if(nom=="") nom="INCONNU"; 
    fprintf(out,"%s %04d ",nom.c_str(),mapper[itt->second->getper()]);
    for(int k=0;k<itt->second->size();k++) {
      fprintf(out,"(%d %s %d) ",(*(itt->second))[k].gare,(*(itt->second))[k].arriv.str().c_str(),(*(itt->second))[k].arret);
    }
    fprintf(out,"\n");
  }
  
  fclose(out);

  out=fopen("passages.txt","w");
  if(out==NULL) {
    _warn("export_txt: ne peut pas creer le fichier\n");
    return;
  }
  
  map<int,map<int,int> >::const_iterator itpa;
  map<int,int>::const_iterator itpa2;
  fprintf(out,"# temps des passages entre 2 gares en dehors du reseau SNCF\n");
  fprintf(out,"# <numero gare 1> <numero gare 2> <temps en minutes>\n");
  for(itpa=dbs.pass.begin();itpa!=dbs.pass.end();++itpa)
    for(itpa2=itpa->second.begin();itpa2!=itpa->second.end();++itpa2)
      fprintf(out,"%d %d %d\n",itpa->first,itpa2->first,itpa2->second);
  
  fclose(out);


  out=fopen("groupes.txt","w");
  if(out==NULL) {
    _warn("export_txt: ne peut pas creer le fichier\n");
    return;
  }
  
  map<int, list<int> >::const_iterator itsu;
  list<int>::const_iterator itsu2;
  fprintf(out,"# groupement des gares (par exemple PARIS est en fait une gare fictive qui regroupe toutes les gares sur Paris)\n");
  fprintf(out,"# <numero gare 1> <numero gare 2>\n");
  for(itsu=dbs.super.begin();itsu!=dbs.super.end();++itsu)
    for(itsu2=itsu->second.begin();itsu2!=itsu->second.end();++itsu2)
      fprintf(out,"%d %d\n",itsu->first,*itsu2);
  
  
  fclose(out);
}
