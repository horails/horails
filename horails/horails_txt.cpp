#include<stdio.h>
#include <sys/time.h>
#include <time.h>


#include "dateheure.hpp"
#include "utils.hpp"
#include "heap.hpp"
#include "db.hpp"
#include "load.hpp"
#include "algo.hpp"
//#include "gui.hpp"

#include "options.hpp"

options_t options;

string gfrom="",gto="";
vector<int> vias;
list<int> garesint;
int sens=1;
int tpscorr=5;
int nbrcorr=0;
dateheure_t dh;
string sd="";
string sh="";

dbs_t *dbs=NULL;
gares_t *gares=NULL;
algo2_t *algo=NULL;



void fct_exclude(const string &s)
{
  int g=gares->searchidbest(s);
  garesint.push_back(g);
}

void fct_via(const string &s)
{
  int g=gares->searchidbest(s);
  vias.push_back(g);
}

void fct_def(const string &s)
{
  if(gfrom=="") {gfrom=s;return;}
  if(gto=="") {gto=s;return;}
}

enum noptions_t {
  O_NO=0,

  O_DEFAULT=1,
  O_FROM,
  O_TO,
  O_TRAIN,
  O_TCORR,
  O_NCORR,
  O_VIA,
  O_EXCLUDE,
  O_REVERSE,
  O_DATE,
  O_TIME,

  O_HELP,
  O_VERB,
  O_QUIET,
};


void init_options() 
{
  options.add(O_DEFAULT,0,"","",O_EXP|-1,OA_FCT_STR,(void*)fct_def);
  
  options.add(O_FROM,'f',"from","gare de depart",1,OA_ARG_STR,&gfrom);
  options.add(O_TO,'t',"to","gare d'arrivée",2,OA_ARG_STR,&gto);

  options.add(O_TCORR,'t',"tc","temps min de correspondance",10,OA_ARG_INT,&tpscorr);
  options.add(O_NCORR,'n',"nc","nbr max de correspondances",11,OA_ARG_INT,&nbrcorr);

  options.add(O_VIA,'v',"via","via gare",15,OA_FCT_STR,(void*)fct_via);
  options.add(O_EXCLUDE,'e',"exclude","exclure gare",16,OA_FCT_STR,(void*)fct_exclude);
  options.add(O_REVERSE,'r',"reverse","sens inverse",20,OA_SET,&sens,-1);
  options.add(O_DATE,'d',"date","date de la recherche",12,OA_ARG_STR,&sd);
  options.add(O_TIME,'h',"heure","heure de la recherche",12,OA_ARG_STR,&sh);

  /*
  options.add(O_HELP,0,"help","print help",10,OA_SET,&help,1);

  options.add(O_MAN,0,"man","print man page",O_EXP|11,OA_SET,&a_help,2);
  options.add(O_EXTRACT,'x',"extract", "extract (an) archive(s)",50,OA_SET,&a_extract,1);
  options.add(O_CREATE,'c',"create","create an archive", 70, OA_SET, &a_create, 1);
  options.add(O_VERB,'v',"verbose", "increment verbosity level",14,OA_ADD,&loglevel,1);
  options.add(O_QUIET,'q',"quiet", "don't print anthing",15,OA_SET,&loglevel,0);
  options.add(O_OUT,'o',"output", "write into 'arg' instead (default: stdout)",72,OA_ARG_STR,&arch_file);
  options.add(O_DIFF,'d',"diff-file","specify the name of the 'diff' file (default: '.incremental-diff'",71,OA_ARG_STR,&diff_file);
  options.add(O_QUICK,'q',"quick");
  options.add(O_EXCLUDE,'e',"exclude","exclude 'arg' from the archive",75,OA_FCT_STR,(void*)fct_exclude);
  options.add(O_EXCLUDEFILE,'E',"exclude-file","exclude files in 'arg' from the archive",76,OA_FCT_STR,(void*)fct_exclude_file);
  options.add(O_NOAUTOEXCLUDE,0,"no-auto-exclude","do not automatically exclude diff file and output archive files",O_EXP|77,OA_SET,&auto_exclude,0);
  options.add(O_PARANO,0,"paranioa","hash every file (even if the last mod time does not change)",O_EXP|80,OA_SET,&quick,0);
  */

  //options.addconflict(O_QUIET,O_VERB);
  //options.addreq(O_OUT,O_CREATE);
}  



static void search(list<score2_t> &lr)
{
  optionsalgo_t d;

  struct timeval tv;
  struct timezone tz;
  
  gettimeofday(&tv,&tz);
  time_t t=tv.tv_sec-tz.tz_minuteswest*60;
  date_t nd(1970,1,1);
  heure_t nh(0,0);
  nd+=(t/60)/1440;
  nh+=(t/60)%1440;

  if(sd!="") {
    int sd_d, sd_m, sd_y ;
    if (sscanf(sd.c_str(), "%d/%d/%d", &sd_d, &sd_m, &sd_y) == 3)
      {
	if (sd_y < 100)
	  sd_y += 2000 ;
	date_t nd2(sd_y, sd_m, sd_d);
	nd = nd2;
      }
  }

  if(sh!="") {
    int sh_h, sh_m ;
    if (sscanf(sh.c_str(), "%d:%d", &sh_h, &sh_m) == 2)
      {
	heure_t nh2(sh_h, sh_m);
	nh = nh2;
      }
  }

  dateheure_t now(nd,nh);
  dh=now;
  printf("Date/heure: %s\n", dh.str().c_str()) ;

  d.gd=gares->searchidbest(gfrom);
  d.ga=gares->searchidbest(gto);
  
  d.tpscorr=tpscorr;
  d.nbrcorr=nbrcorr;

  d.via=vias;
  d.garesint=garesint;
  
  lr=algo->algomoinscon(d,dh,sens);

}

const char *trajet2str(const score2_t &s,const map<char,string>&m) {
  static char buff[100];
  if(s.size()==1) {
    list<trajet_part_t>::const_iterator it;
    it=s.l.begin();
    if(it->train) {
      snprintf(buff,100,"%s",it->train->getname(m).c_str());
      return buff;
    }
    else return  "Passage";
  }
  return "...";
}


void aff(const dbs_t &db,const score2_t &t) 
{
  printf(">%s  \t%s  \t%d  \t%s  \t%s  \t%s  \t%s\n",
	 gares->garestring(t.gare_dep).c_str(),
	 gares->garestring(t.gare).c_str(),
	 t.corr,
	 t.start2.str2().c_str(),
	 t.end2.str2().c_str(),
	 heure_t(0,t.temps_trajet).str().c_str(),
	 trajet2str(t,db.types));

  list<trajet_part_t>::const_iterator it;
  for(it=t.l.begin();it!=t.l.end();++it) {
    printf(" %s  \t%s  \t \t%s  \t%s  \t%s  \t%s\n",
	   gares->garestring(it->gdep).c_str(),
	   gares->garestring(it->garr).c_str(),
	   //NBRC_C, t.corr,
	   it->hdep.str2().c_str(),
	   it->harr.str2().c_str(),
	   heure_t(0,it->harr-it->hdep).str().c_str(),
	   it->train?it->train->getname(db.types).c_str():"Passage");
    //if(it->train)
    //  train2store2(db,iter2,it->train,store,0,it->gdep,it->garr);
  }
  
}

void aff(const dbs_t &db, list<score2_t> &lr)
{
  list<score2_t>::const_iterator it;
  for(it=lr.begin();it!=lr.end();++it)
    aff(db,*it);
}

int main(int argc, char **argv)
{
  try {
    init_options();
    options.process(argc,argv);
    
  } catch (option_error_t &e) {
    _error("'%s --help' for help\n",argv[0]);
    return 1;
  }

  loadconfig();

  _debug("OK...\n");

  dbs_t db;
  load(db);
  dbs=&db;
  gares=&db.gares;
  
  algo2_t aa2(db);
  //aa2.pretraite(db);  
  algo=&aa2;

  list<score2_t> lr;

  search(lr);

  aff(db,lr);

  return 0;
}



#if 0

GtkTreeStore *store=NULL;

GladeXML *gx=NULL;

bool autochange=true;
static void changefieldgare( GtkWidget *widget,
			     gpointer   data )
{
  if(!autochange) return;
  GtkComboBoxEntry *w=(GtkComboBoxEntry *)data;
  //GtkTreeModel* model=gtk_combo_box_get_model(w);  
  //gtk_tree_store_clear(store);
  
  g_print("recherche: %s\n",gtk_entry_get_text((GtkEntry*)widget));
  if(strlen(gtk_entry_get_text((GtkEntry*)widget))<2) return;
  
  list<string> lg=gares->searchstr(string(gtk_entry_get_text((GtkEntry*)widget)));
  list<string>::iterator it; 
 int i=0;
  for(i=0;i<=30;i++) {
    gtk_combo_box_remove_text ((GtkComboBox*)w,0); //beurk comme GTK
  }
  for(it=lg.begin();it!=lg.end();++it) {
    gtk_combo_box_append_text ((GtkComboBox*)w,it->c_str());
  }
  //gtk_combo_box_popup ((GtkComboBox*)w);
  //gtk_combo_box_set_active((GtkComboBox*)w,0);
}

int getsetentrygare(const char* desc)
{
  GtkWidget *entry=glade_xml_get_widget(gx, desc);
  if(entry != NULL && stripspaces(string(gtk_entry_get_text(GTK_ENTRY(entry)))).size()>0) {
    
    int gg=gares->searchidbest(string(gtk_entry_get_text(GTK_ENTRY(entry))));
    string name=gares->garestring(gg);
    gtk_entry_set_text(GTK_ENTRY(entry),name.c_str());
    return gg;
  }
  return 0;
}

static bool run=false;

//static void search_path( GtkWidget *widget, gpointer   data )
static void* search_path(void*)
{
  _debug("search_path\n");

  gdk_threads_enter();

  //g_print("%s\n",gtk_entry_get_text((GtkEntry*)gtk_bin_get_child((GtkBin*)fromfield)));

  GtkWidget *date = glade_xml_get_widget(gx, "gdeDateHeure");
  g_assert(date != NULL);

  _debug("toto\n");

  long ti=gnome_date_edit_get_time(GNOME_DATE_EDIT(date));
  //printf("%d",ti);

  struct tm t;
  
  localtime_r(&ti,&t);
  date_t nd(1900+t.tm_year,t.tm_mon+1,t.tm_mday);
  heure_t nh(t.tm_hour,t.tm_min);
  dateheure_t dh(nd,nh);

  //printf("Date: %s\n",dh.str().c_str());

  optionsalgo_t d;

  GtkWidget *fromfield=glade_xml_get_widget(gx, "geFrom");
  GtkWidget *tofield=glade_xml_get_widget(gx, "geTo");
  g_assert(fromfield != NULL);
  g_assert(tofield !=NULL);

  int gd=getsetentrygare("geFrom");
  //gares->searchidbest(string(gtk_entry_get_text(GTK_ENTRY(fromfield))));
  int ga=getsetentrygare("geTo");
  //gares->searchidbest(string(gtk_entry_get_text(GTK_ENTRY(tofield))));

  d.gd=gd;
  d.ga=ga;
  
  _debug("%d->%d\n",gd,ga);

  if(gd==0 || ga==0) {
    gdk_threads_leave();
    return NULL;
  }
  
  //printf("tpsmin %d %d: %d\n",gd,ga,algo->tempsmin(gd,ga));

  int sens=1;
  d.tpscorr=5;
  d.nbrcorr=0;

  GtkWidget *entrytps=glade_xml_get_widget(gx, "entryTpsCorr");
  if(entrytps != NULL && strlen(gtk_entry_get_text(GTK_ENTRY(entrytps)))>0)
    d.tpscorr=atoi(gtk_entry_get_text(GTK_ENTRY(entrytps)));

  GtkWidget *entrynbr=glade_xml_get_widget(gx, "entryNbrCorr");
  if(entrytps != NULL && strlen(gtk_entry_get_text(GTK_ENTRY(entrynbr)))>0) {
    //printf("nbrcorr='%s'",gtk_entry_get_text(GTK_ENTRY(entrynbr)));
    d.nbrcorr=atoi(gtk_entry_get_text(GTK_ENTRY(entrynbr)));
  }

  int via1=getsetentrygare("entryVia1");
  int via2=getsetentrygare("entryVia2");
  if(via1) d.via.push_back(via1);
  if(via2) d.via.push_back(via2);

  int forb1=getsetentrygare("entryEvite1");
  int forb2=getsetentrygare("entryEvite2");
  if(forb1) d.garesint.push_back(forb1);
  if(forb2) d.garesint.push_back(forb2);

  GtkWidget *rbarr=glade_xml_get_widget(gx, "rbArrive");
  if(rbarr && gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rbarr))) sens=-1;

  gdk_threads_leave();
  
  list<score2_t> lr=algo->algomoinscon(d,dh,sens);

  gdk_threads_enter();

  trajets2store(*dbs,lr,store);

  run=false;

  gdk_threads_leave();

  return NULL;
}


static pthread_t t;

static void search_path_( GtkWidget *widget, gpointer   data )
{
  _debug("search_path_\n");
  //gdk_threads_enter();

  if(run==false) {
    pthread_create (&t, NULL, search_path, NULL);
    run=true;
  }

  //gdk_threads_leave();
  
}

gboolean foo(GtkWidget *widget,GdkEventButton *event, gpointer data) 
{
  GtkWidget *w2=(GtkWidget*)data;
  printf("foo\n");
}

int main(int argc,char *argv[])
{  
  loadconfig();

  _debug("OK...\n");

  dbs_t db;
  load(db);
  dbs=&db;
  gares=&db.gares;
  
  algo2_t aa2(db);
  //aa2.pretraite(db);  
  algo=&aa2;

  GtkWidget *w,*br;

  if(guiglade=="") guiglade="gui.glade";

  g_thread_init(NULL);
  gdk_threads_init();
  //gdk_threads_enter();
  
  gtk_init(&argc, &argv);
  if(guiglade!="" && guiglade[0]=='/')
    gx = glade_xml_new((basedir+"/"+guiglade).c_str(), NULL, NULL);
  else //if(basedir!="") 
    gx = glade_xml_new((basedir+"/"+guiglade).c_str(), NULL, NULL);
  if(gx==NULL) //ancien gui
    gx = glade_xml_new("gui2.glade", NULL, NULL);
  g_assert(gx != NULL);

  glade_xml_signal_autoconnect(gx);

  w = glade_xml_get_widget(gx, "window");
  g_assert(w != NULL);

  g_signal_connect(G_OBJECT(w), "destroy", G_CALLBACK(gtk_main_quit), NULL);
  gtk_widget_show_all(w);


  br = glade_xml_get_widget(gx, "buttonRecherche");
  if(br==NULL) br = glade_xml_get_widget(gx, "bSearch");
  g_assert(br != NULL);

  g_signal_connect (G_OBJECT (br), "clicked", G_CALLBACK (search_path_), NULL);

  GtkWidget *tv= glade_xml_get_widget(gx, "tvResultats");
  g_assert(tv != NULL);
  setup_tree(tv,store);


  //g_signal_connect (G_OBJECT (gtk_bin_get_child((GtkBin*)w)), "changed",
  //		    G_CALLBACK (changefieldgare), w);

  gtk_main();

  //gdk_threads_leave();

  return 0;
}

#endif
