#include "gui.hpp"

const char *trajet2str(const score2_t &s,const map<char,string>&m) {
  static char buff[100];
  if(s.size()==1) {
    list<trajet_part_t>::const_iterator it;
    it=s.l.begin();
    if(it->train) {
      snprintf(buff,100,"%s",it->train->getname(m).c_str());
      return buff;
    }
    else return  "Passage";
  }
  return "...";
}

void train2store(const dbs_t &db, GtkTreeIter iter1, const train_t *t,GtkTreeStore *store, int aff=2,int gd =0,int ga=0)
{
  GtkTreeIter iter2;
  int o=aff;
  const gares_t *gares=&db.gares;
  
  for(int i=0;i<t->size();i++) {
    if(aff<2 && gd==(*t)[i].gare) o=1;    
    if(o) {
      gtk_tree_store_append (store, &iter2, &iter1);
      gtk_tree_store_set (store, &iter2,
			  FROM_C, gares->garestring((*t)[i].gare).c_str(),
			  TO_C, "", //gares->garestring(it->garr).c_str(),
			  //NBRC_C, t.corr,
			  HD_C, i==0?"":(*t)[i].arriv.str().c_str(),
			  HA_C, i==t->size()-1?"":((*t)[i].arriv+(*t)[i].arret).str().c_str(),
			  TT_C, "", //heure_t(0,it->harr-it->hdep).str().c_str(),
			  TRAIN_C, "",
			  -1);
    }
    if(aff<2 && ga==(*t)[i].gare) o=aff;
  }
}

void train2store2(const dbs_t &db, GtkTreeIter iter1, const train_t *t,GtkTreeStore *store, int aff=2,int gd =0,int ga=0)
{
  GtkTreeIter iter2;
  int o=aff;
  const gares_t *gares=&db.gares;
  
  for(int i=0;i<t->size()-1;i++) {
    if(aff<2 && gd==(*t)[i].gare) o=1;
    if(aff<2 && ga==(*t)[i].gare) o=aff;
    if(o) {
      gtk_tree_store_append (store, &iter2, &iter1);
      gtk_tree_store_set (store, &iter2,
			  FROM_C, gares->garestring((*t)[i].gare).c_str(),
			  TO_C, gares->garestring((*t)[i+1].gare).c_str(),
			  //NBRC_C, t.corr,
			  HD_C, ((*t)[i].arriv+(*t)[i].arret).str().c_str(),
			  HA_C, ((*t)[i+1].arriv).str().c_str(),
			  TT_C, heure_t(0,(*t)[i+1].arriv-((*t)[i].arriv+(*t)[i].arret)).str().c_str(),
			  TRAIN_C, "",
			  -1);
    }
  }
}



void trajet2store(const dbs_t &db, const score2_t &t,GtkTreeStore *store)
{
  GtkTreeIter iter1;
  GtkTreeIter iter2;
  const gares_t *gares=&db.gares;
  
  char bcor[5];
  snprintf(bcor,5,"%d",t.corr); //beurk
  gtk_tree_store_append (store, &iter1, NULL);
  gtk_tree_store_set (store, &iter1,
		      FROM_C, gares->garestring(t.gare_dep).c_str(),
		      TO_C, gares->garestring(t.gare).c_str(),
		      NBRC_C, bcor,
		      HD_C, t.start2.str2().c_str(),
		      HA_C, t.end2.str2().c_str(),
		      TT_C, heure_t(0,t.temps_trajet).str().c_str(),
		      TRAIN_C, trajet2str(t,db.types),
		      -1);
  
  list<trajet_part_t>::const_iterator it;
  for(it=t.l.begin();it!=t.l.end();++it) {
    gtk_tree_store_append (store, &iter2, &iter1);
  
    gtk_tree_store_set (store, &iter2,
			FROM_C, gares->garestring(it->gdep).c_str(),
			TO_C, gares->garestring(it->garr).c_str(),
			//NBRC_C, t.corr,
			HD_C, it->hdep.str2().c_str(),
			HA_C, it->harr.str2().c_str(),
			TT_C, heure_t(0,it->harr-it->hdep).str().c_str(),
			TRAIN_C, it->train?it->train->getname(db.types).c_str():"Passage",
			-1);
    if(it->train)
      train2store2(db,iter2,it->train,store,0,it->gdep,it->garr);
  }
}

void trajets2store(const dbs_t &db, const list<score2_t> &l,GtkTreeStore *store)
{
  list<score2_t>::const_iterator it;
  gtk_tree_store_clear(store);
  for(it=l.begin();it!=l.end();++it)
    trajet2store(db,*it,store);
}

GtkWidget *setup_tree (GtkWidget *tree,GtkTreeStore *&store)
{
  //GtkWidget *tree;
   GtkTreeViewColumn *column;
   GtkCellRenderer *renderer;

   /* Create a model.  We are using the store model for now, though we
    * could use any other GtkTreeModel */
   store = gtk_tree_store_new (N_C,
                               G_TYPE_STRING,
                               G_TYPE_STRING,
                               G_TYPE_STRING,
                               G_TYPE_STRING,
                               G_TYPE_STRING,
                               G_TYPE_STRING,
                               G_TYPE_STRING
                               );

   /* Create a view */
   //tree = gtk_tree_view_new_with_model (GTK_TREE_MODEL (store));
   gtk_tree_view_set_model(GTK_TREE_VIEW(tree),GTK_TREE_MODEL (store));

   /* The view now holds a reference.  We can get rid of our own
    * reference */
   g_object_unref (G_OBJECT (store));

   /* Create a cell render and arbitrarily make it red for demonstration
    * purposes */
   renderer = gtk_cell_renderer_text_new ();
   //g_object_set (G_OBJECT (renderer),
   //              "foreground", "red",
   //              NULL);

   /* Create a column, associating the "text" attribute of the
    * cell_renderer to the first column of the model */
   column = gtk_tree_view_column_new_with_attributes ("De", renderer,
                                                      "text", FROM_C,
                                                      NULL);

   /* Add the column to the view. */
   gtk_tree_view_append_column (GTK_TREE_VIEW (tree), column);


   renderer = gtk_cell_renderer_text_new ();
   column = gtk_tree_view_column_new_with_attributes ("A",
                                                      renderer,
                                                      "text", TO_C,
                                                      NULL);
   gtk_tree_view_append_column (GTK_TREE_VIEW (tree), column);

   renderer = gtk_cell_renderer_text_new ();
   column = gtk_tree_view_column_new_with_attributes ("Corr.",
                                                      renderer,
                                                      "text", NBRC_C,
                                                      NULL);
   gtk_tree_view_append_column (GTK_TREE_VIEW (tree), column);

   renderer = gtk_cell_renderer_text_new ();
   column = gtk_tree_view_column_new_with_attributes ("Depart",
                                                      renderer,
                                                      "text",HD_C,
                                                      NULL);
   gtk_tree_view_append_column (GTK_TREE_VIEW (tree), column);

   renderer = gtk_cell_renderer_text_new ();
   column = gtk_tree_view_column_new_with_attributes ("Arrivée",
                                                      renderer,
                                                      "text",HA_C,
                                                      NULL);
   gtk_tree_view_append_column (GTK_TREE_VIEW (tree), column);

   renderer = gtk_cell_renderer_text_new ();
   column = gtk_tree_view_column_new_with_attributes ("Temps",
                                                      renderer,
                                                      "text",TT_C,
                                                      NULL);
   gtk_tree_view_append_column (GTK_TREE_VIEW (tree), column);

   renderer = gtk_cell_renderer_text_new ();
   column = gtk_tree_view_column_new_with_attributes ("Train",
                                                      renderer,
                                                      "text", TRAIN_C,
                                                      NULL);
   gtk_tree_view_append_column (GTK_TREE_VIEW (tree), column);

   return tree;
}



