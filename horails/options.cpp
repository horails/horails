#include "options.hpp"
#include "utils.hpp"

struct option_t {
  option_t(int id_, char c_=0, const string &lo_="", const string &help_="", 
	   int pos_=-1, int action_=OA_NO, void *arg_=NULL, int data_=0):
    id(id_),c(c_),lo(lo_),help(help_),pos(pos_),action(action_),arg(arg_),
    data(data_),o(false) {}
  int id;
  char c;
  string lo;
  string help;
  int pos;
  int action;
  void *arg;
  int data;
  
  list<int> autos;
  list<int> req;
  list<int> conflict;

  int o;
  list<string> sargs;

  int nbrarg() const {
    if(action==OA_ARG_STR ||
       action==OA_ARG_INT ||
       action==OA_FCT_STR) return 1;
    return 0;
  }
  
  void select(const string &s="--") {
    o++;
    if(s!="--") sargs.push_back(s);
  }
  
  void execute() {
    if(action==OA_SET) (*(int*)arg)=data;
    if(action==OA_ADD) (*(int*)arg)+=(o*data);
    if(action==OA_ARG_STR) (*(string*)arg)=*(sargs.begin());
    if(action==OA_ARG_INT) (*(int*)arg)=atoi((sargs.begin())->c_str());
    if(action==OA_FCT_STR) {
      list<string>::const_iterator it;
      for(it=sargs.begin();it!=sargs.end();++it)
	((void(*)(const string &s))arg)(*it);
    }
  }
};

string options_t::nameopt(int id) {
  option_t *o=omap[id];
  if(o==NULL) return "NULL";
  if(o->lo!="") return o->lo;
  char cs[3]="-c";
  if(o->c) {cs[1]=o->c; return cs;} 
  if(o->id==1) return "default";
  return "(unnamed)";
}

void options_t::add(int id, char c, const string &lo, const string &help, 
		    int pos, int action, void *arg, int data)
{
  option_t *o=new option_t(id,c,lo,help,pos,action,arg,data);
  omap[id]=o;
  if(c) omapc[c]=o;
  if(lo!="") omaps[lo]=o;
}

enum noptionsr_t {
  O_CONFLICT,
  O_REQ,
  O_AUTO,
};

void options_t::addr(int r,int i, int a) 
{
  if(a==0) return;
  option_t *o=omap[i];
  if(!o) throw option_error_t();
  if(r==O_CONFLICT) o->conflict.push_back(a);
  if(r==O_AUTO) o->autos.push_back(a);
  if(r==O_REQ) o->req.push_back(a);
}
void options_t::addr(int r, int i, int a,int b, int c,int d,int e)
{
  addr(r,i,a);  addr(r,i,b);  addr(r,i,c);  addr(r,i,d);  addr(r,i,e);
}
void options_t::addconflict(int i, int a,int b, int c,int d,int e)
{
  addr(O_CONFLICT,i,a,b,c,d,e);
}
void options_t::addreq(int i, int a,int b, int c,int d,int e)
{
  addr(O_REQ,i,a,b,c,d,e);
}
void options_t::addauto(int i, int a,int b, int c,int d,int e)
{
  addr(O_AUTO,i,a,b,c,d,e);
}

void options_t::autos() 
{
  map<int,option_t*>::const_iterator it;
  for(it=omap.begin();it!=omap.end();++it)
    if(it->second->o) {
      list<int>::const_iterator iti;
      for(iti=it->second->autos.begin();iti!=it->second->autos.end();++iti)
	if(omap[*iti]) omap[*iti]->o=true;
    }
}


void options_t::verif() 
{
  map<int,option_t*>::const_iterator it;
  for(it=omap.begin();it!=omap.end();++it)
    if(it->second->o) {
      list<int>::const_iterator iti;
      for(iti=it->second->req.begin();
	  iti!=it->second->req.end();++iti)
	if(omap[*iti]==NULL || omap[*iti]->o==false) 
	  throw option_req_error_t(it->first,*iti);
      for(iti=it->second->conflict.begin();
	  iti!=it->second->conflict.end();++iti)
	if(omap[*iti] && omap[*iti]->o==true) 
	  throw option_conflict_error_t(it->first,*iti);
    }
}

void options_t::exec() 
{
  map<int,option_t*>::const_iterator it;
  for(it=omap.begin();it!=omap.end();++it)
    if(it->second->o)
      it->second->execute();
}

void options_t::process(int argc, char **argv) 
{
  try {

  /* read opts...*/
  int i,j;
  bool end=false;
  for(i=1;i<argc;i++) {
    if(!end &&sizeof(argv[i])>=2 && argv[i][0]=='-') {
      if(argv[i][1]=='-') {
	if(argv[i][1]==0) { 
	  end=true;
	} else {
	  string lo=argv[i]+2;
	  option_t *o=omaps[lo];
	  if(o==NULL) throw option_unknown_error_t(string("--")+lo);
	  if(o->nbrarg()==1) {
	    i++;
	    if(i==argc) throw option_arg_error_t(string("--")+lo);
	    o->select(argv[i]);
	  } else 
	    o->select();
	}
      } else {
	for(j=1;argv[i][j];j++) {
	  char c=argv[i][j];
	  char cs[3]="-c";
	  cs[1]=c;
	  option_t *o=omapc[c];
	  if(o==NULL) throw option_unknown_error_t(cs);
	  if(o->nbrarg()==1) {
	    if(argv[i][j+1]!=0) throw option_arg_error_t(cs);
	    i++;
	    if(i==argc) throw option_arg_error_t(cs);
	    o->select(argv[i]);
	    break;
	  } else
	    o->select();
	}
      }
    } else {
      option_t *o=omap[1]; // 1: default option
      if(o==NULL) throw option_unknown_error_t("default");
      o->select(argv[i]);
    }
  }

  autos();
  verif();
  exec();

  } catch (option_unknown_error_t &e) {
    _error("unknown option %s\n",e.s.c_str());
    throw e;
  } catch (option_conflict_error_t &e) {
    _error("conflict between %s and %s\n",nameopt(e.id).c_str(),
	   nameopt(e.id2).c_str());
    throw e;
  } catch (option_error_t &e) {
    _error("error in options\n");
    throw e;
  }
}

void options_t::print_options(int out) 
{
  //TODO
};

  
