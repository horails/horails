#ifndef _GUI_HPP_
#define _GUI_HPP_

#include<stdio.h>
#include<gtk/gtk.h>

#include "algo.hpp"

enum
{
  FROM_C,  //gare depart (S/D)
  TO_C,   // gare arrive (S/D)
  NBRC_C, //nombre de correspondances (S)
  HD_C, //heure depart (S/D)
  HA_C, //heure arivée (S/D)
  TT_C, //temps total  (S)
  TRAIN_C, //type.numero train (D)
  N_C
};

void trajet2store(const dbs_t &db, const score2_t &t,GtkTreeStore *store);
void trajets2store(const dbs_t &db, const list<score2_t> &l,GtkTreeStore *store);
GtkWidget *setup_tree (GtkWidget *tree,GtkTreeStore *&store);

#endif
