#ifndef _HEAP_HPP_
#define _HEAP_HPP_

/*tas binaire avec diminution de valeur*/

template <class T_t,class K_t> class heap_t {
 public:
  class heap_elm_t {
  public:
    heap_elm_t():n(NULL),p(NULL) {}
    heap_elm_t(const T_t &c): elm(c),n(NULL),p(NULL) {}
    T_t elm;
    int i2;
    heap_elm_t *n,*p;
  };
  
  struct heap_elm2_t {
    heap_elm_t *e;
    K_t d;
  };

 private:
  heap_t(const heap_t &) {}
  const heap_t &operator =(const heap_t &h) { return h;}

  int n,n2;
  heap_elm_t el;
  heap_elm2_t *te2;

  void addn(void) {
    if(n2==0) {
      n2=128;
      te2=(heap_elm2_t*)malloc(sizeof(heap_elm2_t)*n2);
    }
    if(n==n2) {
      n2*=2;
      te2=(heap_elm2_t*)realloc(te2,sizeof(heap_elm2_t)*n2);
    }
    n++;
  }

  inline void exch(int a,int b) {
    heap_elm2_t t;
    te2[a].e->i2=b;
    te2[b].e->i2=a;
    t=te2[a];te2[a]=te2[b];te2[b]=t;
  }

 public:
  
  heap_t():n(0),n2(0) {el.n=NULL;}
  ~heap_t() {
    if(n2) free(te2);
    heap_elm_t *t=el.n,*l;
    while(t!=NULL) {
      l=t->n;
      delete l;
      t=l;
    }
  }

#define heap_pr(i) ((((i)+1)/2)-1)
#define heap_ch(i) ((((i)+1)*2)-1)

  int size() const { return n;}
  bool empty() const { return n==0;}
  
  heap_elm_t *push(const T_t &c, const K_t &d) {return add(c,d);}
  heap_elm_t *add(const T_t &c, const K_t &d) {

    heap_elm_t *e=new heap_elm_t(c);
    e->n=el.n;
    e->p=&el;
    if(el.n) 
      el.n->p=e;
    el.n=e;
    
    addn();
    int i=n-1;

    e->i2=i;
    te2[i].e=e;
    te2[i].d=d;
    while(i && te2[i].d<te2[heap_pr(i)].d) {
      exch(i,heap_pr(i));
      i=heap_pr(i);
    }
    return e;
  }

  //heap_elm_t *top();
  T_t pop() {
    T_t r;
    if(n) {
      heap_elm_t *e=te2[0].e;
      r=e->elm;
      if(n>1) {
	int i=0,j;
	exch(0,n-1);
	n--;
	while(1) {
	  j=heap_ch(i);
	  if(j>=n) break;
	  if(j==n-1) {
	    if(te2[j].d<te2[i].d)
	      exch(i,j);
	    break;
	  }
	  if(te2[j+1].d < te2[j].d) j++;
	  if(te2[j].d < te2[i].d) {
	    exch(i,j);
	    i=j;
	    j=heap_ch(i);
	  } else break;
	}
      } else n=0;

      e->p->n=e->n;
      if(e->n)
	e->n->p=e->p;
      delete e; 
    }
    return r;
  }

  void remove(heap_elm_t *e) {
    int i=e->i2;
    while(i) {
      exch(i,heap_pr(i));
      i=heap_pr(i);
    }
    pop();
  }
  
  void dec(heap_elm_t *e, const K_t &d) {
    int i=e->i2;
    te2[i].d=d;
    while(i && te2[i].d<te2[heap_pr(i)].d) {
      exch(i,heap_pr(i));
      i=heap_pr(i);
    }
  }
  
};

#endif
