#ifndef _OPTION_HPP_
#define _OPTION_HPP_

#include <string>
#include <map>
using namespace std;

#include "utils.hpp"

class option_error_t : public exception_t {
public:
  option_error_t(int i=0):id(i) {}
  option_error_t(const string &s_):s(s_) {}
  int id;
  string s;
};
class option_req_error_t : public option_error_t {
public:
  option_req_error_t(int i=0,int j=0):option_error_t(i),id2(j) {}
  int id2;
};
class option_conflict_error_t : public option_error_t {
public:
  option_conflict_error_t(int i=0,int j=0):option_error_t(i),id2(j) {}
  int id2;
};
class option_unknown_error_t: public option_error_t {
public:
  option_unknown_error_t(const string &s):option_error_t(s) {}
};
class option_arg_error_t: public option_error_t {
public:
  option_arg_error_t(const string &s):option_error_t(s) {}
};

class option_t;

#define O_EXP 0
//#define O_DEFAULT 1

enum option_action_t {
  OA_NO,
  OA_SET,
  OA_ADD,
  OA_ARG_STR,
  OA_ARG_INT,
  OA_FCT_STR,
  OA_END
};

class options_t {
public:
  void add(int id, char c=0, const string &lo="", 
	   const string &help="", int pos=-1, 
	   int action=OA_NO, void *arg=NULL, int data=0);
  
  void addconflict(int i, int a,int b=0, int c=0,int d=0,int e=0);
  void addreq(int i, int a,int b=0, int c=0,int d=0,int e=0);
  void addauto(int i, int a,int b=0, int c=0,int d=0,int e=0);
  
  void process(int argc, char **argv);

private:
  map<int,option_t*> omap;
  map<char,option_t*> omapc;
  map<string,option_t*> omaps;

  void addr(int r,int i, int a); 
  void addr(int r, int i, int a,int b, int c,int d,int e);

  void verif();
  void autos();
  void exec();
  void print_options(int out=2);
  string nameopt(int id);
};

#endif
