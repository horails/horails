#include<stdio.h>
#include<gtk/gtk.h>
#include<glade/glade.h>
#include<libgnomeui/libgnomeui.h>

#include "dateheure.hpp"
#include "utils.hpp"
#include "heap.hpp"
#include "db.hpp"
#include "load.hpp"
#include "algo.hpp"
#include "gui.hpp"

dbs_t *dbs=NULL;
gares_t *gares=NULL;
algo2_t *algo=NULL;

GtkTreeStore *store=NULL;

GladeXML *gx=NULL;

bool autochange=true;
static void changefieldgare( GtkWidget *widget,
			     gpointer   data )
{
  if(!autochange) return;
  GtkComboBoxEntry *w=(GtkComboBoxEntry *)data;
  //GtkTreeModel* model=gtk_combo_box_get_model(w);  
  //gtk_tree_store_clear(store);
  
  g_print("recherche: %s\n",gtk_entry_get_text((GtkEntry*)widget));
  if(strlen(gtk_entry_get_text((GtkEntry*)widget))<2) return;
  
  list<string> lg=gares->searchstr(string(gtk_entry_get_text((GtkEntry*)widget)));
  list<string>::iterator it; 
 int i=0;
  for(i=0;i<=30;i++) {
    gtk_combo_box_remove_text ((GtkComboBox*)w,0); //beurk comme GTK
  }
  for(it=lg.begin();it!=lg.end();++it) {
    gtk_combo_box_append_text ((GtkComboBox*)w,it->c_str());
  }
  //gtk_combo_box_popup ((GtkComboBox*)w);
  //gtk_combo_box_set_active((GtkComboBox*)w,0);
}

int getsetentrygare(const char* desc)
{
  GtkWidget *entry=glade_xml_get_widget(gx, desc);
  if(entry != NULL && stripspaces(string(gtk_entry_get_text(GTK_ENTRY(entry)))).size()>0) {
    
    int gg=gares->searchidbest(string(gtk_entry_get_text(GTK_ENTRY(entry))));
    string name=gares->garestring(gg);
    gtk_entry_set_text(GTK_ENTRY(entry),name.c_str());
    return gg;
  }
  return 0;
}

static bool run=false;

//static void search_path( GtkWidget *widget, gpointer   data )
static void* search_path(void*)
{
  _debug("search_path\n");

  gdk_threads_enter();

  //g_print("%s\n",gtk_entry_get_text((GtkEntry*)gtk_bin_get_child((GtkBin*)fromfield)));

  GtkWidget *date = glade_xml_get_widget(gx, "gdeDateHeure");
  g_assert(date != NULL);

  _debug("toto\n");

  long ti=gnome_date_edit_get_time(GNOME_DATE_EDIT(date));
  //printf("%d",ti);

  struct tm t;
  
  localtime_r(&ti,&t);
  date_t nd(1900+t.tm_year,t.tm_mon+1,t.tm_mday);
  heure_t nh(t.tm_hour,t.tm_min);
  dateheure_t dh(nd,nh);

  //printf("Date: %s\n",dh.str().c_str());

  optionsalgo_t d;

  GtkWidget *fromfield=glade_xml_get_widget(gx, "geFrom");
  GtkWidget *tofield=glade_xml_get_widget(gx, "geTo");
  g_assert(fromfield != NULL);
  g_assert(tofield !=NULL);

  int gd=getsetentrygare("geFrom");
  //gares->searchidbest(string(gtk_entry_get_text(GTK_ENTRY(fromfield))));
  int ga=getsetentrygare("geTo");
  //gares->searchidbest(string(gtk_entry_get_text(GTK_ENTRY(tofield))));

  d.gd=gd;
  d.ga=ga;
  
  _debug("%d->%d\n",gd,ga);

  if(gd==0 || ga==0) {
    gdk_threads_leave();
    return NULL;
  }
  
  //printf("tpsmin %d %d: %d\n",gd,ga,algo->tempsmin(gd,ga));

  int sens=1;
  d.tpscorr=5;
  d.nbrcorr=0;

  GtkWidget *entrytps=glade_xml_get_widget(gx, "entryTpsCorr");
  if(entrytps != NULL && strlen(gtk_entry_get_text(GTK_ENTRY(entrytps)))>0)
    d.tpscorr=atoi(gtk_entry_get_text(GTK_ENTRY(entrytps)));

  GtkWidget *entrynbr=glade_xml_get_widget(gx, "entryNbrCorr");
  if(entrytps != NULL && strlen(gtk_entry_get_text(GTK_ENTRY(entrynbr)))>0) {
    //printf("nbrcorr='%s'",gtk_entry_get_text(GTK_ENTRY(entrynbr)));
    d.nbrcorr=atoi(gtk_entry_get_text(GTK_ENTRY(entrynbr)));
  }

  int via1=getsetentrygare("entryVia1");
  int via2=getsetentrygare("entryVia2");
  if(via1) d.via.push_back(via1);
  if(via2) d.via.push_back(via2);

  int forb1=getsetentrygare("entryEvite1");
  int forb2=getsetentrygare("entryEvite2");
  if(forb1) d.garesint.push_back(forb1);
  if(forb2) d.garesint.push_back(forb2);

  GtkWidget *rbarr=glade_xml_get_widget(gx, "rbArrive");
  if(rbarr && gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rbarr))) sens=-1;

  gdk_threads_leave();
  
  list<score2_t> lr=algo->algomoinscon(d,dh,sens);

  gdk_threads_enter();

  trajets2store(*dbs,lr,store);

  run=false;

  gdk_threads_leave();

  return NULL;
}


static pthread_t t;

static void search_path_( GtkWidget *widget, gpointer   data )
{
  _debug("search_path_\n");
  //gdk_threads_enter();

  if(run==false) {
    pthread_create (&t, NULL, search_path, NULL);
    run=true;
  }

  //gdk_threads_leave();
  
}

gboolean foo(GtkWidget *widget,GdkEventButton *event, gpointer data) 
{
  GtkWidget *w2=(GtkWidget*)data;
  printf("foo\n");
}

int main(int argc,char *argv[])
{  
  loadconfig();

  _debug("OK...\n");

  dbs_t db;
  load(db);
  dbs=&db;
  gares=&db.gares;
  
  algo2_t aa2(db);
  //aa2.pretraite(db);  
  algo=&aa2;

  GtkWidget *w,*br;

  if(guiglade=="") guiglade="gui.glade";

  g_thread_init(NULL);
  gdk_threads_init();
  //gdk_threads_enter();
  
  gtk_init(&argc, &argv);
  if(guiglade!="" && guiglade[0]=='/')
    gx = glade_xml_new((basedir+"/"+guiglade).c_str(), NULL, NULL);
  else //if(basedir!="") 
    gx = glade_xml_new((basedir+"/"+guiglade).c_str(), NULL, NULL);
  if(gx==NULL) //ancien gui
    gx = glade_xml_new("gui2.glade", NULL, NULL);
  g_assert(gx != NULL);

  glade_xml_signal_autoconnect(gx);

  w = glade_xml_get_widget(gx, "window");
  g_assert(w != NULL);

  g_signal_connect(G_OBJECT(w), "destroy", G_CALLBACK(gtk_main_quit), NULL);
  gtk_widget_show_all(w);


  br = glade_xml_get_widget(gx, "buttonRecherche");
  if(br==NULL) br = glade_xml_get_widget(gx, "bSearch");
  g_assert(br != NULL);

  g_signal_connect (G_OBJECT (br), "clicked", G_CALLBACK (search_path_), NULL);

  GtkWidget *tv= glade_xml_get_widget(gx, "tvResultats");
  g_assert(tv != NULL);
  setup_tree(tv,store);


  //g_signal_connect (G_OBJECT (gtk_bin_get_child((GtkBin*)w)), "changed",
  //		    G_CALLBACK (changefieldgare), w);

  gtk_main();

  //gdk_threads_leave();

  return 0;
}

