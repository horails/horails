#!/bin/bash

if test "$1" = "" ;then
echo "usage:" $0 "<repertoire de destination>"
exit 1
fi

cd /tmp
if test ! -d horails_ete08 ; then mkdir horails_ete08 ;fi
cd horails_ete08

wget http://sebastien.martin2.free.fr/sncf/pexcsncf.exe
#wget http://lwdr.free.fr/doc/horaires-pc.zip
#cp /tmp/horaires-pc.zip .

#md5sum horaires-pc.zip
if test "$(md5sum pexcsncf.exe | cut -d " " -f 1)" != "b9194956f3053da5f6d0a8079717d2e9"; then
echo "erreur: mauvais hash."
cd ..
rm -rf horails_ete08
exit 1
fi

unzip pexcsncf.exe PEXC.ZIP
rm pexcsncf.exe 
unzip PEXC.ZIP "ETE-GL*"
rm PEXC.ZIP
mv ETE-GL ETE-2008
mv ETE-2008 $1
cd ..
rm -rf horails_ete08

exit 0
