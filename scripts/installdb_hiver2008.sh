#!/bin/bash

if test "$1" = "" ;then
echo "usage:" $0 "<repertoire de destination>"
exit 1
fi

cd /tmp
if test ! -d horails_hiver08 ; then mkdir horails_hiver08 ;fi
cd horails_hiver08

wget http://lwdr.free.fr/doc/horaires_14-12-08.zip
#wget http://sebastien.martin2.free.fr/sncf/pexcsncf.exe
#wget http://lwdr.free.fr/doc/horaires-pc.zip
#cp /tmp/horaires-pc.zip .

#md5sum horaires-pc.zip
if test "$(md5sum horaires_14-12-08.zip | cut -d " " -f 1)" != "c17f532f65a2d2c3e4530cef4e0763eb"; then
echo "erreur: mauvais hash."
cd ..
rm -rf horails_hiver08
exit 1
fi

unzip horaires_14-12-08.zip
unzip horaires_14-12-08.exe PXHC.ZIP
rm horaires_14-12-08.exe
unzip PXHC.ZIP "hiver-gl/*"
rm -f PXHC.ZIP
mv hiver-gl HIVER-2008
cd HIVER-2008
for f in * ; do mv $f $(echo $f | tr [a-z] [A-Z]) ; done
cd ..
mv HIVER-2008 $1
cd ..
rm -rf horails_hiver08

exit 0
