#!/bin/bash

if test "$1" = "" ;then
echo "usage:" $0 "<repertoire de destination>"
exit 1
fi

cd /tmp
if test ! -d horails_hiver10 ; then mkdir horails_hiver10 ;fi
cd horails_hiver10

wget http://info-rail.fr/SNCF/DONNEES/HIVER/DonneesRihoVH071210.exe
#wget http://sebastien.martin2.free.fr/sncf/pexcsncf.exe
#wget http://lwdr.free.fr/doc/horaires-pc.zip
#cp /tmp/horaires-pc.zip .

#md5sum horaires-pc.zip
if test "$(md5sum DonneesRihoVH071210.exe | cut -d " " -f 1)" != "e5b26dfe0ee1d9994be1a6ee469cf6c6"; then
echo "erreur: mauvais hash."
cd ..
rm -rf horails_hiver10
exit 1
fi

mkdir HIVER-2010
cd HIVER-2010
unzip ../DonneesRihoVH071210.exe
for f in * ; do mv $f $(echo $f | tr [a-z] [A-Z]) ; done
cd ..
mv HIVER-2010 $1
cd ..
rm -rf horails_hiver10

exit 0
