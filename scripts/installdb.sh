#!/bin/bash

QUIET=0
YES=0

while getopts "qy" flag
do
  if test "$flag" = "q" ; then QUIET=1 ;fi
  if test "$flag" = "y" ; then YES=1 ;fi
done

if test "$UID" = "0" ;then
 DDEST="/usr/share/horails"
 DCFG="/etc/horails"
else
 if [[ $QUIET = 0 ]] ;then
  echo "L'instalation en tant que 'root' permettra à tous les utilisateurs de pouvoir utiliser 'horails'."
 fi

 DDEST="$HOME/.horails"
 DCFG="$HOME/.horails"
fi

if [[ $QUIET = 0 ]] ;then
  echo "Instalation dans $DDEST."
  echo "Instalation du fichier de configuration dans $DCFG."
fi

if test ! -d $DDEST ; then mkdir -p $DDEST ;fi
if test ! -d $DCFG ; then mkdir -p $DCFG ;fi

if test ! -d $DDEST/HIVER-2010 ; then ./installdb_hiver2010.sh $DDEST ;fi
if test -f $DCFG/horails.conf ; then rm $DCFG/horails.conf ;fi
touch "$DCFG/horails.conf"
echo "BASE $DDEST" > "$DCFG/horails.conf"
echo "GUI gui.glade" >> "$DCFG/horails.conf"
echo "RIHO $DDEST/HIVER-2010" >> "$DCFG/horails.conf"
cp gui.glade $DDEST/
